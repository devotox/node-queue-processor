require! {
	\assert
	\should
	_: \lodash
	Promise: \bluebird
}

(require \mocha-as-promised)!

test = it

root = "../"

process.env.TEST = yes
process.env.DEBUG = no

require "#{root}src/libs/date"
config = require "#{root}src/config"
utils = require "#{root}src/classes/utils"

Api = require "#{root}src/classes/Api"
Email = require "#{root}src/classes/Email"
Queue = require "#{root}src/classes/Queue"
Server = require "#{root}src/classes/Server"
Profile = require "#{root}src/classes/Profile"
Process = require "#{root}src/classes/Process"
DataDump = require "#{root}src/classes/DataDump"
Template = require "#{root}src/classes/Template"
Tokenizer = require "#{root}src/classes/Tokenizer"
Publisher = require "#{root}src/classes/Publisher"
Validator = require "#{root}src/classes/Validator"
Permission = require "#{root}src/classes/Permission"
Submission = require "#{root}src/classes/Submission"
Escalation = require "#{root}src/classes/Escalation"
Integration = require "#{root}src/classes/Integration"
Notification = require "#{root}src/classes/Notification"

test_app_path = "#{root}src/server/test"
test_app = require test_app_path

test_filename = \AF-Test-1234567890
test_uri = "sandbox://#{test_filename}"
test_customer = "eeda5230-55b2-4b74-b2f0-d4fd37711bba"
test_email = [ <[ Devonte test.email@firmstep.com To]> ]

api_config = _.extend {}, config.api, customer_id: test_customer
queue_config = config.queue

notification_config = 
	API: api_config
	LIM: id: config.lim.id
	template: 
		priority: null
		attachments: null
		subject: "Firmstep Notification"
		message: "<p>!!!Firmstep Notification!!!</p>"
		html: true, type: \email, recipients: []
		from: [ \notifications@firmstep.com, \Firmstep ]

tokenizer_config = 
	tokens: 
		first: \first
		second: \second
	env_tokens: 
		env_first: \env_first
		env_second: \env_second
	global_tokens: 
		global_first: \global_first
		global_second: \global_second
	values: 
		sectionA: 
			fieldA: 
				name: \fieldA
				value: \SectionA-FieldA
			fieldB:
				name: \fieldB
				value: \SectionA-FieldB
		sectionB:
			fieldC:
				name: \fieldC
				value: \SectionB-FieldC
			fieldD:
				name: \fieldD
				value: \SectionB-FieldD
		sectionC:
			fieldE:
				name: \fieldE
				value: \SectionC-FieldE

############################ TESTS ################################

describe 'Utils Tests' ->
	@timeout 0
	test 'To Base 64' ->
		dec = \test
		enc = utils.toBase64 dec
		enc.should.eql \dGVzdA==

	test 'From Base 64' ->
		enc = \dGVzdA==
		dec = utils.fromBase64 enc
		dec.should.eql \test

	test 'Short UUID' ->
		id = utils.uuid!
		id.length.should.eql 6

describe 'Tokenizer Tests' ->
	@timeout 0
	test 'No Submission Error' ->
		try new Tokenizer!
		catch e then e.message.should.eql \NoSubmissionError

	test 'No Submission Values Error' ->
		try new Tokenizer do
			values: {}, env_tokens: { abc: \abc }
		catch e then e.message.should.eql \NoValuesError

	test 'No Submission Tokens Error' ->
		try new Tokenizer do
			values: { abc : \abc}, env_tokens: { abc: \abc }
		catch e then e.message.should.eql \NoTokensError

	test 'No Submission Env Tokens Error' ->
		try new Tokenizer do
			values: { abc : \abc }, tokens: { abc: \abc }
		catch e then e.message.should.eql \NoEnvTokensError

	test 'No Submission Global Tokens Error' ->
		try new Tokenizer do
			values: { abc : \abc }, tokens: { abc: \abc }, env_tokens: { abc: \abc }
		catch e then e.message.should.eql \NoGlobalTokensError

	test 'Set Field Token' ->
		t = new Tokenizer tokenizer_config

		t.setFieldValue('fieldE', \ValueChanged)

		t.getValue('fieldE')

		.should.eql \ValueChanged


	test 'Get Field Token' ->
		new Tokenizer tokenizer_config

		.getValue('fieldA')

		.should.eql \SectionA-FieldA

	test 'Get Env Token' ->
		new Tokenizer tokenizer_config

		.getValue('env_first')

		.should.eql \env_first

	test 'Get AF Token' ->
		new Tokenizer tokenizer_config

		.getValue('today_en')

		.should.eql Date.today().toString('dd/MM/yyyy')

	test 'Get Local Token' ->
		new Tokenizer tokenizer_config

		.getValue('first')

		.should.eql \first

	test 'Get Global Token' ->
		new Tokenizer tokenizer_config

		.getValue('global_first')

		.should.eql \global_first

	test 'Get Token Array (String)' ->
		val = new Tokenizer tokenizer_config

		.getTokens '{fieldA} is equal to {fieldA}'

		val.should.be.an.instanceOf Array and
		val.length.should.eql 1 and
		val[0].should.eql '{fieldA}'

	test 'Get Token Array (Object)' ->
		val = new Tokenizer tokenizer_config

		.getTokens text: '{fieldA} is equal to {fieldA}', value: '{fieldD} is not {fieldB}', arr: [ '{fieldD}', '{fieldC}' ]

		val.should.be.an.instanceOf Array and
		val.length.should.eql 4 and
		val[0].should.eql '{fieldA}'

	test 'Replace Tokens' ->
		val = new Tokenizer tokenizer_config

		.replaceTokens '{fieldD} is equal to {env_second}'
		
		val.should.eql 'SectionB-FieldD is equal to env_second'

	test 'Parameterize Tokens' ->		
		val = new Tokenizer tokenizer_config

		.parameterizeTokens '{fieldD} is equal to {env_second} but {fieldE}'

		val.should.be.an.instanceOf Array and
		val.length.should.eql 2 and
		val[0].should.eql ':fieldD is equal to :env_second but :fieldE' and 
		val[1].should.be.an.instanceOf Array


describe 'API Tests' ->
	@timeout 0
	test 'No Docapi URL Error' ->
		try testapi = new Api do
			apibroker_url: "", docapi_url: "", customer_id: ""
		catch e then e.message.should.eql \NoDocapiUrlError

	test 'No Customer ID Error' ->
		try testapi = new Api do
			apibroker_url: "", docapi_url: \abc, customer_id: ""
		catch e then e.message.should.eql \NoCustomerIdError

	test 'No API Broker URL Error' ->
		try testapi = new Api do
			apibroker_url: "", docapi_url: \abc, customer_id: \abc
		catch e then e.message.should.eql \NoApibrokerUrlError

	test 'Invalid Docapi URL Error' ->
		try testapi = new Api do
			apibroker_url: \abc, docapi_url: \abc, customer_id: \abc
		catch e then e.message.should.eql "InvalidDocapiUrlError: abc"

	test 'Invalid API Broker URL Error' ->
		try testapi = new Api do
			apibroker_url: \abc, docapi_url: "http://localhost/docapi", customer_id: \abc
		catch e then e.message.should.eql "InvalidApibrokerUrlError: abc"

	test 'No URI in Docapi Request Error' ->
		testapi = new Api api_config
		testapi.docapi ''
		.catch (e) -> e.message.should.eql \NoUriInDocapiRequestError

	test 'No API Selected Error' ->
		testapi = new Api api_config
		testapi.get ''
		.catch (e) -> e.message.should.eql \NoApiSelectedError

describe 'Doc API Tests' ->
	@timeout 0
	test 'Put Document' ->
		Promise.delay 0
		.then ->
			testapi = new Api api_config
			testapi.docapi test_uri, 'put', content: \dGVzdA==, force: 1, metadata: {type: \test-data}
		.then (body) ->
			throw new Error body.Error if body.Error
			throw new Error \NoResponsePath unless body?.path
			body.path?.should.eql test_filename

	test 'Get Document' ->
		Promise.delay 500
		.then ->
			testapi = new Api api_config
			testapi.docapi test_uri
		.then (body) ->
			throw new Error body.Error if body.Error
			throw new Error \NoResponseBody unless body?.metadata and body?.content
			body.metadata.type.should.eql \test-data and
			body.metadata.filename.should.eql test_filename

	test 'Delete Document' ->
		Promise.delay 500
		.then ->
			testapi = new Api api_config
			testapi.docapi test_uri, 'delete'
		.then (body) ->
			throw new Error body.Error if body.Error
			throw new Error \NoResponseBody unless body
			body.should.be.an.instanceOf Object and
			body.path.should.eql test_filename
			
	test 'Get Document After Delete' ->
		Promise.delay 500
		.then ->
			testapi = new Api api_config
			testapi.docapi test_uri
		.then (body) ->
			throw new Error body.Error if body.Error
			throw new Error \NoResponseBody unless body?.metadata and body?.content
			body.metadata.should.be.empty and
			body.content.should.startWith '<?xml version="1.0"'

describe 'API Broker Tests' ->
	@timeout 0

	test 'No Customer ID in URL returns 403 Response' ->
		Promise.delay 0
		.then ->
			testapi = new Api _.extend {}, api_config
			testapi.get \TestNONE
		.catch (e) ->
			e.toString().should.eql "Error: ResponseError: 403"

	test 'Test GET' ->
		Promise.delay 0
		.then ->
			testapi = new Api api_config
			testapi.get \TestNONE
		.then (body) ->
			body.should.have.property.1

	test 'Test POST' ->
		Promise.delay 0
		.then ->
			testapi = new Api api_config
			testapi.post \TestNONE
		.then (body) ->
			body.should.have.property.1

	test 'Test PUT' ->
		Promise.delay 0
		.then ->
			testapi = new Api api_config
			testapi.put \TestNONE
		.then (body) ->
			body.should.have.property.1

describe 'LIM Connect (Notification)' ->
	@timeout 0
	test 'Url Not Defined Error' ->
		try testnote = new Notification do
			LIM: key: \key, iv: \iv
		catch e then e.message.should.eql \UrlNotDefinedError
		
	test 'Key Not Defined Error' ->
		try testnote = new Notification do
			LIM: url: \url, iv: \iv
		catch e then e.message.should.eql \KeyNotDefinedError
	
	test 'IV Not Defined Error' ->	
		try testnote = new Notification do
			LIM: url: \url, key: \key
		catch e then e.message.should.eql \IVNotDefinedError

describe 'Notification Tests' ->
	@timeout 0
	test 'Lim Connect Errors' ->
		try testnote = new Notification do
			LIM: key: \key, iv: \iv, id: null
		catch e then e.message.should.eql \UrlNotDefinedError
		
		try testnote = new Notification do
			LIM: url: \url, iv: \iv, id: null
		catch e then e.message.should.eql \KeyNotDefinedError
		
		try testnote = new Notification do
			LIM: url: \url, key: \key, id: null
		catch e then e.message.should.eql \IVNotDefinedError

	test 'No Message Error' ->
		try 
			testnote = new Notification do
				template: {}, API: notification_config.API
			testnote.send test_email
			.caught (e) -> e.message.should.eql \NoMessageError
		catch e then e.message.should.eql \NoMessageError

	test 'No Recipient Error' ->
		try 
			testnote = new Notification do
				template: { message: \message }, API: notification_config.API
			testnote.send!
			.caught (e) -> e.message.should.eql \NoRecipientError
		catch e then e.message.should.eql \NoRecipientError

	test 'No Sender Error' ->
		try 
			testnote = new Notification do
				template: { message: \message }, API: notification_config.API
			testnote.send test_email
			.caught (e) -> e.message.should.eql \NoSenderError
		catch e then e.message.should.eql \NoSenderError

	test 'Send Notification (Get LIM)' ->
		testnote = new Notification notification_config, false, true

		testnote.send test_email, message: "<p>Test Notification GET LIM</p>"

		.then (data) -> data.should.eql "Email message was successfully sent."

		.caught (e) -> e.message.should.eql "RejectionError: getaddrinfo ENOTFOUND"

describe 'Email Tests' ->
	test 'No Site Error' ->
		try new Email!
		catch e then e.message.should.eql \NoSiteError

	test 'No Save Error' ->
		try new Email do
			site: \abc
		catch e then e.message.should.eql \NoSaveIdError

	test 'No Form Error' ->
		try new Email do
			site: \abc, 'save-id': \AF-Save-abc
		catch e then e.message.should.eql \NoFormError

	test 'No User Error' ->
		try new Email do
			site: \abc, 'save-id': \AF-Save-abc, 'form-name': \AF-Form
		catch e then e.message.should.eql \NoUserEmailError

describe 'Queue Tests' ->
	@timeout 0

	test 'No Queue Name Error' ->
		try queue = new Queue do 
			region: \region, secretAccessKey: \secret, accessKeyId: \access
		catch e then e.message.should.eql \NoQueueNameError

	test 'No Region Error' ->
		try queue = new Queue do 
			name: \name, secretAccessKey: \secret, accessKeyId: \access
		catch e then e.message.should.eql \NoRegionError

	test 'No Secret Key Error' ->
		try queue = new Queue do 
			name: \name, region: \region, accessKeyId: \access
		catch e then e.message.should.eql \NoSecretKeyError

	test 'No Access Key Error' ->
		try queue = new Queue do 
			name: \name, region: \region, secretAccessKey: \secret
		catch e then e.message.should.eql \NoAccessKeyError

describe 'Server Tests' ->
	
	test 'No Host Error' ->
		try server = new Server do 
			port: \port, host: ''
		catch e then e.message.should.eql \NoHostError
	
	test 'No Port Error' ->
		try server = new Server do 
			host: \host, port: ''
		catch e then e.message.should.eql \NoPortError

	test 'Invalid Port Error' ->
		try server = new Server do 
			host: \host, port: \port
		catch e then e.message.should.eql \InvalidPortError

	test 'No Paths Error' ->
		try 
			server = new Server!
			server.set!
		catch e then e.message.should.eql \NoPathsError

	test 'Invalid Path Error' ->
		try 
			server = new Server!
			server.set <[ abc ]>
		catch e then e.message.should.eql "InvalidPathError: abc"

	test 'No App Error' ->
		try 
			server = new Server!
			server.setApp!
			server.start()
		catch e then e.message.should.eql \NoAppsError

	test 'App Not A Function Error' ->
		try 
			server = new Server!
			server.setApp <[ abc ]>
		catch e then e.message.should.eql "AppNotFunctionError: abc"

	test 'No Apps or Paths Set Error' ->
		try 
			server = new Server!
			server.start()
		catch e then e.message.should.eql \NoAppsSetError

	test 'No Server Started' ->
		try
			server = new Server!
			server.setApp test_app
			server.stop!
		catch e then e.message.should.eql \ServerNotStartedError

	test 'Valid App' ->
		server = new Server!
		server.setApp test_app
		server.start!
		server.stop!

describe 'Publisher Tests' ->
	@timeout 0
	test 'No Customer ID Error' ->
		try testpublisher = new Publisher!
		catch e then e.message.should.eql \NoCustomerIdError

	test 'No Document Type Error' ->
		try testpublisher = new Publisher do
			customer_id: \abc
		catch e then e.message.should.eql \NoDocumentTypeError

	test 'No Form Endpoint Error' ->
		try testpublisher = new Publisher do
			form_endpoint: '', type: 'form', customer_id: \abc
		catch e then e.message.should.eql \NoFormEndpointError

	test 'No Process Endpoint Error' ->
		try testpublisher = new Publisher do
			process_endpoint: '', type: 'form', customer_id: \abc
		catch e then e.message.should.eql \NoProcessEndpointError

	test 'No Publish Endpoint Error' ->
		try testpublisher = new Publisher do
			publish_endpoint: '', type: 'form', customer_id: \abc	
		catch e then e.message.should.eql \NoPublishEndpointError

	test 'No Archive Endpoint Error' ->
		try testpublisher = new Publisher do
			archive_endpoint: '', type: 'form', customer_id: \abc	
		catch e then e.message.should.eql \NoArchiveEndpointError

	test 'API Errors (Publisher)' ->
		try testpublisher = new Publisher do		
			apibroker_url: "", docapi_url: "", customer_id: "", type: 'form'
		catch e then e.message.should.eql \NoCustomerIdError

describe 'Submission Tests' ->
	@timeout 0
	test 'No Customer ID Error' ->
		try testpublisher = new Submission!
		catch e then e.message.should.eql \NoCustomerIdError

	test 'No Forms Endpoint Error' ->
		try testpublisher = new Submission do
			customer_id: \abc, forms_endpoint: ''
		catch e then e.message.should.eql \NoFormsEndpointError

	test 'No Files Endpoint Error' ->
		try testpublisher = new Submission do
			customer_id: \abc, files_endpoint: ''
		catch e then e.message.should.eql \NoFilesEndpointError

	test 'No Submission Endpoint Error' ->
		try testpublisher = new Submission do
			customer_id: \abc, files_endpoint: \endpoint, submission_endpoint: ''
		catch e then e.message.should.eql \NoSubmissionEndpointError

	test 'API Errors (Submission)' ->
		try testsubmission = new Submission do
			customer_id: \abc, files_endpoint: \endpoint, submission_endpoint: \endpoint, submission: { formValues: \abc }, apibroker_url: "", docapi_url: ""
		catch e then e.message.should.eql \NoDocapiUrlError

describe 'Integration Tests' ->
	@timeout 0
	test 'No Submission Error' ->
		try new Integration!
		catch e then e.message.should.eql \NoSubmissionError

	test 'No Integration Error' ->
		try new Integration do 
			submission: { formValues: {} }
		catch e then e.message.should.eql \NoIntegrationError

	test 'API Errors (Integration)' ->
		try new Integration do	
			apibroker_url: "", docapi_url: "", customer_id: "", submission: { formValues: {} }, integration: \abc
		catch e then e.message.should.eql \NoDocapiUrlError

describe 'Process Tests' ->
	@timeout 0
	test 'No Stage Error' ->
		try new Process!
		catch e then e.message.should.eql \NoStageError

	test 'No Process Error' ->
		try new Process do
			stage: { values: {} }
		catch e then e.message.should.eql \NoProcessError

	test 'No Reference Error' ->
		try new Process do
			process: { values: {} }, stage: { values: {} }
		catch e then e.message.should.eql \NoReferenceError

	test 'No Submission Error' ->
		try new Process do
			reference: \AF-9999, process: { values: {} }, stage: { values: {} }
		catch e then e.message.should.eql \NoSubmissionError

	test 'API Errors (Process)' ->
		try new Process do	
			apibroker_url: "", docapi_url: "", customer_id: "", reference: \AF-9999
			process: { values: {} }, stage: { values: {} }, submission: { formValues: {} }
		catch e then e.message.should.eql \NoDocapiUrlError

	test 'Tokenizer Errors (Process)' ->
		try new Process do	
			customer_id: api_config.customer_id, reference: \AF-9999
			process: { values: {} }, stage: { values: {} }, submission: { formValues: {}, env_tokens: {} }
		catch e then e.message.should.eql \NoValuesError

describe 'Escalation Tests' ->
	@timeout 0
	test 'No Docapi URL Error' ->
		try new Escalation do
			apibroker_url: "", docapi_url: "", customer_id: ""
		catch e then e.message.should.eql \NoDocapiUrlError

	test 'No Customer ID Error' ->
		try new Escalation do
			apibroker_url: "", docapi_url: \abc, customer_id: ""
		catch e then e.message.should.eql \NoCustomerIdError

	test 'No API Broker URL Error' ->
		try new Escalation do
			apibroker_url: "", docapi_url: \abc, customer_id: \abc
		catch e then e.message.should.eql \NoApibrokerUrlError

describe 'Validator Tests' ->
	@timeout 0
	test 'No Customer ID Error' ->
		try new Validator!
		catch e then e.message.should.eql \NoCustomerIdError

	test 'No Form Error' ->
		try new Validator do
			customer_id: \abc
		catch e then e.message.should.eql \NoFormError

	test 'No Submission Error' ->
		try new Validator do
			customer_id: \abc, form: { sections: {} }
		catch e then e.message.should.eql \NoSubmissionError

	test 'API Errors (Validator)' ->
		try new Validator do	
			apibroker_url: "", docapi_url: "", customer_id: \abc,
			form: { sections: {} }, submission: { formValues: {} }
		catch e then e.message.should.eql \NoDocapiUrlError

describe 'Profile Tests' ->
	test 'No Profile Action Error' ->
		try new Profile!
		catch e then e.message.should.eql \NoProfileActionError

	test 'Invalid Profile Action Error' ->
		try new Profile do 
			action: \blah
		catch e then e.message.should.eql \InvalidProfileActionError

	test 'API Errors (Profile)' ->
		try new Profile do	
			apibroker_url: "", docapi_url: "", customer_id: "", action: "get"
		catch e then e.message.should.eql \NoDocapiUrlError

perm_args = 
	user_email: \simon.brown@firmstep.com
	user_groups: [
		"USERGROUP-b7e563ac-ab72-4aff-c249-0dbaffa6ebb9",
		"USERGROUP-b75fe564-da04-4f77-90a2-00627b7681d1"
	]

describe 'Permission Tests' ->
	test 'No Permission Action Error' ->
		try new Permission!
		catch e then e.message.should.eql \NoPermissionActionError

	test 'Invalid Permission Action Error' ->
		try new Permission do 
			action: \blah
		catch e then e.message.should.eql \InvalidPermissionActionError

	test 'API Errors (Permission)' ->
		try new Permission do	
			apibroker_url: "", docapi_url: "", customer_id: "", action: "list_groups"
		catch e then e.message.should.eql \NoDocapiUrlError

	test 'List Groups' ->
		perm = new Permission do	
			customer_id: test_customer, action: "list_groups"
		perm.run perm_args
		.then -> it.should.be.an.instanceOf Array

	test 'List Users' ->
		perm = new Permission do	
			customer_id: test_customer, action: "list_users"
		perm.run perm_args
		.then -> it.should.be.an.instanceOf Array

	test 'List Groups For User' ->
		perm = new Permission do	
			customer_id: test_customer, action: "list_groups_for_user"
		perm.run perm_args
		.then -> it.should.be.an.instanceOf Array

	test 'List Users In Groups' ->
		perm = new Permission do	
			customer_id: test_customer, action: "list_users_in_groups"
		perm.run perm_args
		.then -> it.should.be.an.instanceOf Array

describe 'Template Tests' ->
	@timeout 0
	test 'No Template Name Error' ->
		try new Template!
		catch e then e.message.should.eql \NoTemplateNameError

describe 'Data Dump Tests' ->
	test_form = test_sub = null

	@timeout 0
	test 'No Docapi URL Error' ->
		try new DataDump do
			apibroker_url: "", docapi_url: "", customer_id: ""
		catch e then e.message.should.eql \NoDocapiUrlError

	test 'No Customer ID Error' ->
		try new DataDump do
			apibroker_url: "", docapi_url: \abc, customer_id: ""
		catch e then e.message.should.eql \NoCustomerIdError

	test 'No API Broker URL Error' ->
		try new DataDump do
			apibroker_url: "", docapi_url: \abc, customer_id: \abc
		catch e then e.message.should.eql \NoApibrokerUrlError

	/*test 'Get Cases' ->
		new DataDump do
			customer_id: test_customer
		.getCases! .bind @
		.then (data) ->
			data.should.be.an.instanceOf Array

	test 'Get Tasks' ->
		new DataDump do
			customer_id: test_customer
		.getTasks! .bind @
		.then (data) ->
			data.should.be.an.instanceOf Array

	test 'Get Submissions' ->
		new DataDump do
			customer_id: test_customer
		.getSubmissions! .bind @
		.then (data) ->
			test_form := data[0]?['form-uri']
			test_sub := data[0]?['submission-uri']
			data.should.be.an.instanceOf Array

	test 'Get Submission Doc' ->
		new DataDump do
			customer_id: test_customer
		.getDocument test_sub .bind @
		.then (data) ->
			data?.content?.should.be.an.instanceOf Object and
			data?.metadata?.should.be.an.instanceOf Object
		.caught (e) -> e.message.should.eql \NoUriInDocapiRequestError
	*/

describe 'Data Dump SQL Tests' ->
	test_id = 'test_id'
	test_ref = 'test_ref'
	test_drop_column = 'name'
	test_add_column = 'first_name'
	test_table = 'knex_test_table'
	test_table2 = 'knex_test_table_two'
	test_sql = 'create table `test_table` (`ID` int unsigned not null auto_increment primary key, `Status` varchar(200), `Reference` varchar(200), `created_date` varchar(200), `completed_date` varchar(200), `text` varchar(200), `subform` varchar(200), `text1` varchar(200))'

	@timeout 0
	test 'SQL Create Table' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.createTable test_table, null, test_id
		table.should.eql "create table `#{test_table}` (`#{test_id}` int unsigned not null auto_increment primary key)"

	test 'SQL Delete Table' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.deleteTable test_table
		table.should.eql "drop table `#{test_table}`"

	test 'SQL Delete Table If Exists' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.deleteTable test_table, true
		table.should.eql "drop table if exists `#{test_table}`"

	test 'SQL Rename Table' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.renameTable test_table, test_table2
		table.should.eql "rename table `#{test_table}` to `#{test_table2}`"

	test 'SQL Alter Table' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.alterTable test_table, [ { name: test_add_column }, { name: test_drop_column, drop: true } ]
		table.should.eql "alter table `#{test_table}` add `#{test_add_column}` varchar(200);\nalter table `#{test_table}` drop `#{test_drop_column}`"

	test 'SQL Delete Row' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.deleteRow test_table, test_id, test_ref
		table.should.eql "delete from `#{test_table}` where `ID` = '#{test_id}' or (`ID` is null and `Reference` = '#{test_ref}')"

	test 'SQL Insert Row' ->
		table = new DataDump do
			customer_id: test_customer
		.sql!.insertRow test_table, id: test_id, ref: test_ref 
		table.should.eql "insert into `#{test_table}` (`id`, `ref`) values ('#{test_id}', '#{test_ref}')"

	test 'SQL Create convert to mssql' ->
		table = new DataDump do
			customer_id: test_customer
		q = table.sql!.convert test_sql
		q.should.eql "create table [test_table] ([ID] int check ([ID] > 0) not null identity primary key, [Status] varchar(200), [Reference] varchar(200), [created_date] varchar(200), [completed_date] varchar(200), [text] varchar(200), [subform] varchar(200), [text1] varchar(200))"

	/*test 'Prepare Tasks' ->
		table = new DataDump do
			customer_id: test_customer
		table.prepareTasks! 
		.then -> 
			table.tasks_table_create_sql?.should.be.an.instanceOf Object and
			table.tasks_table_insert_sql?.should.be.an.instanceOf Array and
			table.tasks_table_insert_sql?[0].should.have.property \ins and
			table.tasks_table_insert_sql?[0].should.have.property \del

	test 'Prepare Cases' ->
		table = new DataDump do
			customer_id: test_customer
		table.prepareCases!
		.then -> 
			table.cases_table_create_sql?.should.be.an.instanceOf Object and
			table.cases_table_insert_sql?.should.be.an.instanceOf Array and
			table.cases_table_insert_sql?[0].should.have.property \ins and
			table.cases_table_insert_sql?[0].should.have.property \del

	test 'Prepare Submissions' ->
		table = new DataDump do
			customer_id: test_customer
		table.prepareSubmissions! 
		.then ->
			table.submissions_table_create_sql?.should.be.an.instanceOf Object and
			table.submissions_table_insert_sql?.should.be.an.instanceOf Array and
			table.submissions_table_insert_sql?[0].should.have.property \ins and
			table.submissions_table_insert_sql?[0].should.have.property \del

	test 'Prepare All' ->
		table = new DataDump do
			print_data: yes
			customer_id: test_customer
		table.prepareAll!
		.then -> 
			table.all_table_alter_sql?.should.be.an.instanceOf Array*/

	/*test.only 'Run Test Dump' ->
		table = new DataDump do
			print_data: yes
			customer_id: test_customer
		table.run!
	/**/

