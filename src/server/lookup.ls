

module.exports = (app, options, @debug=process.env.DEBUG, @test=process.env.TEST) ->	
	throw new Error 'No Express App' unless app

	util = require \util
	Integration = require "./../classes/Integration"
	console.info "['Server'] ['Lookup'] App Created @ /lookup"	

	app.post \/lookup, (req, res) -> 
		timeStart = +new Date!

		_error = (err) -> 
			hs = if is_test then 200 else 500
			console.error "['Server'] ['Lookup']", err
			res.send hs, status: \error, error: err, lookup_id: lookup_id, runtime: +new Date! - timeStart

		lookup_id = req.param 'id'
		is_test = !!req.param 'test'
		customer = req.param 'customer'
		repeat_against = req.param 'repeat_against'
		submission = req.param('submission') or '{}'
		try submission = JSON.parse submission
		
		console.info "['Server'] ['Lookup']", "Running lookup @ #{lookup_id}" 

		if @debug and not @test
			console.info "['Server'] ['Lookup']", "ID: #{lookup_id}, Test: #{is_test}, Repeat Against: #{repeat_against}" 
			console.info "['Server'] ['Lookup']", "customer: #{customer}, submission: #{util.inspect submission }"

		if not lookup_id or lookup_id is '{get/id}'
			return _error new Error "Invalid Lookup ID"

		opts = 
			isLookup: yes
			throwError: yes
			isTest: is_test
			return_values: yes
			debug_runtimes: yes
			customer_id: customer
			submission: submission
			integration: [ id: lookup_id, repeat_against: repeat_against ]
			global_tokens: 
				_customer_: customer
				_summary_: submission.summary_fields
				json_data: JSON.stringify submission.formValues
				reference: "AF-Lookup-#{lookup_id}-#{timeStart}"

		try i = new Integration opts, @debug, @test

		catch err then return _error err

		i.run! .bind @ 
		.spread (response, transformed, sub_values, runtimes) ->
			i.current.limConnect ?= _duration: 0
			lim_data = 
				request: i.current.limConnect._request
				response: i.current.limConnect._response
				duration: i.current.limConnect._duration
				pre_transform: i.current.limConnect.pre_transform
				post_transform: i.current.limConnect.post_transform
				# json_response: i.current.limConnect._json_response

			int_data = 
				id: lookup_id
				response: response
				type: i.current.type
				transformed: transformed
				tokens: i.current.tokens
				template: i.current.out_template
				calculated_template: i.calc_template 
				pre_transform: i.current.limConnect.pre_transform
				post_transform: i.current.limConnect.post_transform
				runtimes:
					integration: runtimes
					api: +new Date! - timeStart + \ms
					lim: i.current.limConnect._duration + \ms					

			to_send = status: \done, data: lim_data.response, integration: int_data
			to_send.lim_data = lim_data if opts.isTest or (@debug and not @test)

			console.info "['Server'] ['Lookup']", "Lookup @ #{lookup_id} Completed" 
			res.send 200, to_send

		.caught _error
