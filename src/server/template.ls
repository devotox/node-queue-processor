

module.exports = (app, options, @debug=process.env.DEBUG, @test=process.env.TEST) ->	
	throw new Error 'No Express App' unless app

	util = require \util
	Template = require "./../classes/Template"
	console.info "['Server'] ['Template'] App Created @ /template"	

	app.post \/template, (req, res) -> 

		_error = (err) -> 
			console.error "['Server'] ['Template']", err
			res.send 404, status: \error, error: err

		template = req.param 'template'
		context = req.param 'context' or '{}'
		try context = JSON.parse context
		
		console.info "['Server'] ['Template']", "Getting Template @ #{template}" 

		if @debug and not @test
			console.info "['Server'] ['Template']", "Context:", context 

		if not template or template is '{get/template}'
			res.send 500, status: "Invalid Template Name", template: template

		opts = 
			context: context
			template: template

		try t = new Template opts, @debug, @test

		catch err then return _error err

		t.render! .bind @ .then (html) ->
			console.info "['Server'] ['Template']", "HTML:", html if @debug and not @test
			console.info "['Server'] ['Template']", "#{template} template Created"
			res.send 200, template: template, html: html

		.caught _error
		
