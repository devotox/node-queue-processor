
module.exports = (app, options, @debug=process.env.DEBUG, @test=process.env.TEST) ->
	throw new Error 'No Express app' unless app
	console.info "Test App Created! @ /test" unless @test
	
	app.all \/test, (req, res) -> 
		res.send "Test Passed"
