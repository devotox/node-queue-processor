
require! {
	Promise: \bluebird
}

docroot = "../../../../"
API = require \../classes/Api
utils = require \../classes/utils
LimConnect = require "#{docroot}FS-LIM-Connect/build/src/app"

module.exports = (app, options, @debug=process.env.DEBUG, @test=process.env.TEST) ->	
	throw new Error 'No Express App' unless app

	console.info "['Server'] ['Integration'] App Created @ /integration"

	app.post \/integration, (req, res) -> 
		timeStart = +new Date!

		_error = (err) -> 
			hs = if is_test then 200 else 500
			console.error "['Server'] ['Integration']", err
			res.send hs, status: \error, error: err, runtime: +new Date! - timeStart

		_getLim = ->			
			new API customer_id: customer
			
			.get "/lims", { action: 'get', id: lim } .bind @ .then -> it?[0] or it

			.caught -> throw new Error "['No LIM Error'] ID: #{lim_id} || Error: #{it.message or it}"

		lim = req.param 'lim'
		is_test = !!req.param 'test'
		customer = req.param 'customer'
		integration_xml = req.param 'xml'
		
		console.info "['Server'] ['Integration']", "Running integration XML..." 

		if @debug and not @test
			console.info "['Server'] ['Integration']", "XML", "\n", integration_xml, "\n"

		if not lim
			return _error new Error "No LIM ID"
		else if not customer
			return _error new Error "No Customer ID"
		else if not integration_xml
			return _error new Error "No Integration XML"

		Promise.resolve yes .bind @ .then _getLim

		.then -> new LimConnect it, @debug, @test .xml utils.fromBase64 integration_xml

		.then ->
			console.info "['Server'] ['Integration']", "Integration Run Completed!" 
			to_send = status: \done, data: it, runtime: +new Date! - timeStart
			res.send 200, to_send

		.caught _error
