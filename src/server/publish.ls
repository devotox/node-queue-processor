

module.exports = (app, options, @debug=process.env.DEBUG, @test=process.env.TEST) ->	
	throw new Error 'No Express App' unless app

	Publisher = require "./../classes/Publisher"
	console.info "['Server'] ['Publish'] App Created @ /publish"	

	ucfirst = (str) -> [ str.charAt 0 .toUpperCase!, str.substr 1 ].join ''

	app.post \/publish, (req, res) -> 

		_error = (err) -> 
			hs = if is_test then 200 else 500
			console.error "['Server'] ['Publish']", err
			res.send hs, status: \error, error: err, uri: uri, runtime: +new Date! - timeStart

		timeStart = +new Date!
		uri = req.param 'uri'
		type = req.param 'type'
		action = req.param 'action'
		is_test = !!req.param 'test'
		customer = req.param 'customer'
		user = 
			id: req.param 'user-id'
			name: req.param 'user-display-name'

		_type = ucfirst type
		_action = ucfirst action 

		console.info "['Server'] ['Publish']", "#{_action}ing #{_type} from #{uri} by #{user.name}" 

		if @debug and not @test
			console.info "['Server'] ['Publish']", "URI: #{uri}, Action: #{action}, Test: #{is_test}, customer: #{customer}"

		if not uri or uri is '{get/id}'
			return _error new Error "Invalid #{type} uri"

		opts = 
			delay: 0
			type: type
			throwError: yes
			isTest: is_test
			customer_id: customer

		p_opts = 
			uri: uri
			user: user
			action: action

		try p = new Publisher opts, @debug, @test

		catch err then return _error err

		p.run p_opts .bind @ .spread (name, data) ->
			console.info "['Server'] ['Publish']", "#{_action}ed #{_type} #{name}" 
			console.info "['Server'] ['Publish']", "Data:\n", data if data and @debug and not @test
			
			to_send = status: \done, data: data, name: name, uri: uri, runtime: +new Date! - timeStart
			res.send 200, to_send
		.caught _error
		
