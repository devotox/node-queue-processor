require! {
	_: \lodash
	bugsnag: \bugsnag
	Promise: \bluebird
}

root = "../../"

config = require "#{root}build/src/config"
utils = require "#{root}build/src/classes/utils"

Email = require "#{root}build/src/classes/Email"
Queue = require "#{root}build/src/classes/Queue"
Server = require "#{root}build/src/classes/Server"
DataDump = require "#{root}build/src/classes/DataDump"
Publisher = require "#{root}build/src/classes/Publisher"
Escalation = require "#{root}build/src/classes/Escalation"
Submission = require "#{root}build/src/classes/Submission"
Notification = require "#{root}build/src/classes/Notification"

{ArgumentParser} = require \argparse

parser = new ArgumentParser do
	description: \FS-Node
	addHelp: yes

parser.addArgument <[ -d --debug ]>,
	help: 'Set node to debug mode.'
	required: no

parser.addArgument <[ -q --queue ]>,
	help: 'Set node queue.'
	required: no

console_args = parser.parseArgs!

config.debug = !!(console_args.debug or config.debug) && console_args.debug isnt \false
config.queue.name = console_args.queue if console_args.queue
process.env.DEBUG = yes if config.debug

notify = (err, customer, data) ->
	return unless customer

	note = 
		API: _.merge { customer_id: customer }, _.clone config.api
		LIM: id: config.lim.id
		template: 
			subject: "FS-Node Error"
			recipients: [ <[ Devonte devonte.emokpae@firmstep.com To]> ]
			html: true, type: \email, from: [ \errors@firmstep.com, "Firmstep Errors" ]
			message: [ 			
				"<h3>Queue Name: ", config?.queue?.name, "</h3>"
				"<pre>Error Message: ", err?.message or err, "</pre>
				<pre>", err?.stack, "</pre>"
				"<pre>Queue Data:</pre><pre>", ( if data then JSON.stringify data, null, 4 ), "</pre>" 
			].join ''

	new Notification note .send!

	.then (msg) ->
		console.info "['Error Notify'] #{msg}"

	.caught (err) ->
		console.error "['Error Notify'] Send Error:", err

errorHandler = (err, e, prefix, doNotify=true) ->

	_err = if err instanceof Error then err else new Error err

	if doNotify and err?.message isnt \NoUserEmailError
		customer = e?.data?['customer-id'] or e?.data?.customer_id
		bugsnag.notify _err, data: e.data, errorName: prefix
		try notify _err, customer, e.data
	
	err = err?.toString!
	console.error prefix, "['Initialize']", err if prefix
	console.error prefix, "['Queue Data']", e.data if e and prefix and config.debug
	e.deleteMessage! if e and ( not doNotify or ( prefix and prefix isnt "['Submit']" ) )

	if err.indexOf('not a valid email address') > -1
		console.info "['Error Handler']", "Invalid Email Address Handled..."

	else if err.indexOf('NoUserEmailError') > -1
		console.info "['Error Handler']", "No User Email Error Handled..."

	else if err.indexOf('NoTempUriError') > -1
		console.info "['Error Handler']", "No Temp URI Error Handled..."

	else if err.indexOf('InvalidPaymentHash') > -1
		console.info "['Error Handler']", "Invalid Payment Hash Error Handled..."

actions = 
	publish: (e) ->
		console.info "['Publisher'] #{e.data['reference']} Started..."

		_error = null
		p = new Publisher do
			type: e.data['document-type']
			customer_id: e.data['customer-id']
			archive_endpoint: e.data['archive-endpoint']
			publish_endpoint: e.data['document-endpoint']

		p.publish do
			uri: e.data['document-uri']
			reference: e.data['reference']
			user: 
				id: e.data['user-id']
				email: e.data['user-email'] or ''
				name: e.data['user-display-name']

		.then (name) ->
			console.info "['Publisher'] Document #{name} Published!"
			e.deleteMessage!

		.caught (err) ->
			errorHandler err, e, null, err?.message isnt \NoPublishDocumentError
			_error := err unless err?.message is \NoPublishDocumentError
			console.error "['Publisher'] Error:", err?.message or err

		.lastly -> p.delete! unless _error

	submit: (e) ->
		console.info "['Submission'] #{e.data['reference']} started..."
		
		_error = null
		s = new Submission do
			customer_id: e.data['customer-id']
			forms_endpoint: e.data['forms-endpoint']
			files_endpoint: e.data['files-endpoint']
			submission_endpoint: e.data['submission-endpoint']

		s.submit do 
			created: e.data['created']
			reference: e.data['reference']
			uri: e.data['document-uri'] or e.data['submission-uri']
			user: 
				id: e.data['citizen-id'] or e.data['user-id'] or '0'
				email: e.data['citizen-email'] or e.data['user-email'] or ''
				name: e.data['citizen-display-name'] or e.data['user-display-name'] or 'Anonymous'
			csa: 
				id: if e.data['citizen-id'] then e.data['user-id']
				email: if e.data['citizen-id'] then e.data['user-email']
				name: if e.data['citizen-id'] then e.data['user-display-name']

		.spread (reference, uri) ->
			if uri then console.info "['Submission'] #{reference} stored @ #{uri}"
			else console.info "['Submission'] #{reference} not stored"
			e.deleteMessage!

		.caught (err) ->
			errorHandler err, e, null, err?.message isnt \NoSubmitDocumentError
			_error := err unless err?.message is \NoSubmitDocumentError
			console.error "['Submission'] Error:", err?.message or err

		.lastly -> s.delete! unless _error

	email: (e) ->
		console.info "['Email'] Email Started..."

		new Email e.data .send!

		.spread (email, link) ->
			return console.info "['Email'] Successfully not sent to #{email}" if link is false
			console.info "['Email'] Successfully sent to #{email}"
			console.info "['Email'] Save Link: #{link}" if link

		.caught (err) ->
			console.error "['Email'] Error:", err?.message or err
			errorHandler err, e

		.lastly -> e.deleteMessage!

	notification: (e) ->
		console.info "['Notification'] Notification Started..."

		new Notification e.data .send!

		.then (msg) ->
			console.info "['Notification'] #{msg}"

		.caught (err) ->
			console.error "['Notification'] Error:", err?.message or err
			errorHandler err, e

		.lastly -> e.deleteMessage!

	escalation: (e) ->
		console.info "['Escalation'] Escalation Started..."

		args = customer_id: config.escalation.body.customer_id

		new Escalation args .run!

		.then (list) ->
			console.info "['Escalation'] #{list.length} Tasks Escalated"

		.caught (err) ->
			console.error "['Escalation'] Error:", err?.message or err
			errorHandler err, e

		.lastly -> e.deleteMessage!

	dump: (e) ->
		console.info "['Data Dump'] Started..."

		new DataDump e.data .run!

		.spread (customer, client) ->
			console.info "['Data Dump'] #{customer} Data Dumped in a #{client} DB"

		.caught (err) ->
			console.error "['Data Dump'] Error:", err?.message or err
			errorHandler err, e	

		.lastly -> e.deleteMessage!

init =
	bugsnag: (_config = config.bugsnag) ->

		console.info "['Bugsnag'] Setting up #{_config.stack} stack with key #{_config.api_key}"
		
		bugsnag.register _config.api_key,
			releaseStage: _config.stack
		
	server: (_config = config.server) ->

		s = new Server _config

		console.info "['Server'] running @ port #{_config.port}..."

		s.set _config.apps

		try s.start!
		catch err then errorHandler err, null, "['Server']"

	queue: (_config = config.queue) ->

		q = new Queue _config

		console.info "['Queue'] #{_config.name} running..."
		console.info "['Queue'] Press (ctrl + c) to quit..."

		# q.add config.escalation

		q.on "saved-form-email", (e) ->
			try actions.email e
			catch err then errorHandler err, e, "['Email']"

		q.on "submit", (e) ->
			try actions.submit e
			catch err then errorHandler err, e, "['Submit']"

		q.on "publish", (e) ->
			try actions.publish e
			catch err then errorHandler err, e, "['Publish']"

		q.on "data-dump", (e) ->
			try actions.dump e
			catch err then errorHandler err, e, "['Data Dump']"

		q.on "escalation", (e) ->
			try actions.escalation e
			catch err then errorHandler err, e, "['Escalation']"

		q.on "notification", (e) ->
			try actions.notification e
			catch err then errorHandler err, e, "['Notification']"

_.each init, (v) -> do v


