
require! {
	fs: \fs
	_: \lodash
	Promise: \bluebird
	dust: \dustjs-helpers
}

h = require("./../libs/af.dust.helpers")
h(_, dust)

module.exports = class Template

	defaults = 
		context: {}
		template: null
		templates_path: "../FS-R4/www/renderer/scripts/template.js"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Template Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoTemplateNameError unless @options.template

		eval fs.readFileSync @options.templates_path, 'utf-8'

	render: -> Promise.promisify(dust.render)(@options.template, @options.context or {})