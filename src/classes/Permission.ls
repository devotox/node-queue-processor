
require! {
	fs: \fs
	_: \lodash
	Promise: \bluebird
	param: \node-jquery-param
}

API = require "./Api"
require "./../libs/date"
utils = require "./utils"

module.exports = class Permission
	settings = 
		list_users_type: \listusers
		list_groups_type: \listgroups
		permissions_api: \/userPermissions
		add_user_to_groups_type: \addusertogroups
		list_groups_for_user_type: \listgroupsforuser
		list_users_in_groups_type: \listusersingroups
		remove_user_from_groups_type: \removeuserfromgroups
		no_transform_types: <[ add_user_to_groups remove_user_from_groups ]>
		allowed_actions: <[ list_groups list_users list_groups_for_user list_users_in_groups add_user_to_groups remove_user_from_groups ]>

	defaults = 
		customer_id: null
		docapi_url: \http://localhost/docapi/
		apibroker_url: \http://localhost/apibroker/

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Permission Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoPermissionActionError unless @options.action
		throw new Error \InvalidPermissionActionError unless @options.action in settings.allowed_actions

		@api = new API @options, @debug, @test

	run: (args) ->
		Promise.resolve yes .bind @	

		.then -> @[@options.action] args

		.then -> @_error it

		.then -> @_transform it

		.caught -> error: if _.isObject ( err = it?.message or it ) then JSON.stringify err else err

	list_users: ->
		@api.get settings.permissions_api, type: settings.list_users_type

	list_groups: ->
		@api.get settings.permissions_api, type: settings.list_groups_type

	list_groups_for_user: ->
		@api.get settings.permissions_api, type: settings.list_groups_for_user_type, user_email: it?.user_email

	list_users_in_groups: ->
		@api.get settings.permissions_api, type: settings.list_users_in_groups_type, user_email: it?.user_email, user_groups: it?.user_groups

	remove_user_from_groups: ->
		@api.post settings.permissions_api, { type: settings.remove_user_from_groups_type }, { user_email: it?.user_email, user_groups: it?.user_groups }

	add_user_to_groups: ->
		@api.post settings.permissions_api, { type: settings.add_user_to_groups_type }, { user_email: it?.user_email, groups: @_makeGroups it?.user_groups } 

	_error: ->  throw new Error it.error if it?.error; it

	_makeGroups: (groups) -> _.map groups, (v) -> value: v

	_transform: -> 
		return [ it ] if @options.action in settings.no_transform_types

		ret = 
			rows_data: @_makeRowsList it
			select_data: @_makeSelectList it
			
		return [ it, ret ]

	_makeSelectList: (obj={}, ret=[]) ->
		return ret unless _.size obj
		ret = _.map obj, (v={}) -> label: v.name, value: v.user_id or v.value
		return ret

	_makeRowsList: (obj={}, ret={}) ->
		return ret unless _.size obj
		_.each obj, (v={}) -> 
			id = v.user_id or v.group_id or v.value
			ret[id] = ID: id, UCRN: v.ucrn,  Name: v.name, Email: v.user_email
		return ret
