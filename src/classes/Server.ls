
require! {
	_: \lodash
	events: \events
	express: \express
	bugsnag: \bugsnag
}

module.exports = class Server

	defaults = 		
		port: "8124"
		host: "127.0.0.1"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Server Options \n', options

		@apps = []
		@options = _.defaults options, defaults

		throw new Error \NoHostError unless @options.host
		throw new Error \NoPortError unless @options.port
		throw new Error \InvalidPortError unless _.isNumber Number @options.port and not isNaN Number @options.port

		@app = @setup_server!

	setup_server: ->
		app = express!
		app.use bugsnag.errorHandler
		app.use bugsnag.requestHandler
		app.use express.json limit: '100mb'
		app.use express.urlencoded limit: '100mb'

		app.all "/*", (req, res, next) ~>
			@allowCrossDomain req, res, next
		return app

	allowCrossDomain: (req, res, next) ->
		res.header "Access-Control-Allow-Origin", "*"
		res.header "Access-Control-Allow-Headers", "Content-Type"
		res.header "Access-Control-Allow-Headers", "X-Requested-With"
		res.header "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS"
		next!

	start: (port=@options.port) ->
		throw new Error \NoAppsSetError unless @apps.length

		@_server = @app.listen port

	stop: ->
		throw new Error \ServerNotStartedError unless @_server
		@_server.close()

	set: (paths=[]) ->
		@paths = _.flatten [ paths ]
		throw new Error \NoPathsError unless @paths.length

		@paths.map ~> 
			try @apps.push require("#{process.cwd()}/#{it}") @app, @options, @debug, @test
			catch e then throw new Error "InvalidPathError: #{it}"

	setApp: (apps=[]) ->		
		apps = _.flatten [apps]
		throw new Error \NoAppsError unless apps?.length

		apps.map ~> 
			try 
				throw new Error "AppNotFunctionError: #it" unless _.isFunction it 
				@apps.push it @app, @options, @debug, @test
			catch e then throw new Error e.message or "InvalidAppError"




