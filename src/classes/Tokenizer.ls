
require! {
	_: \lodash
	jsdom: \jsdom
	uuid: \node-uuid
	libxmljs: \libxmljs
	querystring: \querystring
}

require "./../libs/date"
utils = require "./utils"
$ = require("jquery")( jsdom.jsdom().parentWindow )

module.exports = class Tokenizer

	settings = 
		html_token_types: <[ unhiddensummary hiddensummary allsummary allfields alltokens ]>
		html_in_value_types: <[ mapSingle mapMultiple textarea subform ]>
		updateTypes: <[ database http webservice ]>
		mapTypes: <[ mapSingle mapMultiple ]>
		encodingKeys: <[ payload body ]>
		xml_token_types: <[ xml_data ]>
		google: 
			api_key: "AIzaSyDMYR3xNkRwUG-OsKEC4WVi1ptWIPzybMs"
			map_size: "400x200"
			map_type: "roadmap"
			map_zoom: "14"
			map_scale: "1"
		format: 
			datetime: "dd/MM/yyyy HH:mm"
			date: "dd/MM/yyyy"
			time: "HH:mm"

	defaults = 
		values: null
		tokens: null
		env_tokens: null
		global_tokens: null

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Tokenizer Options \n', options

		throw new Error \NoSubmissionError unless options and not _.isEmpty options

		@options = _.defaults options, defaults

		throw new Error \NoEnvTokensError unless @options.env_tokens
		throw new Error \NoValuesError unless @options.values and not _.isEmpty @options.values
		throw new Error \NoTokensError unless @options.tokens and not _.isEmpty @options.tokens
		throw new Error \NoGlobalTokensError unless @options.global_tokens and not _.isEmpty @options.global_tokens

		@standardizeTokens!

	standardizeTokens: ->
		tokens = @options.tokens
		@af_tokens =
			valid: 'valid'
			invalid: 'invalid'

			alltokens: ( ~>
				try @_fromForm 'tokens'
				catch then null
			)!

			allfields: ( ~>
				try @_fromForm 'fields'
				catch then null
			)!

			allsummary: ( ~>
				try @_fromForm 'summary'
				catch then null
			)!

			unhiddensummary: ( ~>
				try @_fromForm 'summary', 1
				catch then null
			)!

			hiddensummary: ( ~>
				try @_fromForm 'summary', 2
				catch then null
			)!

			now: new Date!
			nowms: Date.now!
			nownotz: new Date!.toUTCString!

			today_en: Date.today!.toString('dd/MM/yyyy')
			today_us: Date.today!.toString('MM/dd/yyyy')
			today: Date.today!.toString(settings.format.date)
			tomorrow: Date.today!.add(1).days().toString(settings.format.date)
			yesterday: Date.today!.add(-1).days().toString(settings.format.date)

			current_time: new Date!.toString(settings.format.time)
			current_date: new Date!.toString(settings.format.date)
			current_datetime: new Date!.toString(settings.format.datetime)

			uuid: uuid.v4!
			guid: uuid.v4!.replace /\-/gmi, ""
			randomstring:  uuid.v4!.replace(/\-/gmi, "").substr(0, 11)

			host: tokens.host
			port: tokens.port
			site_url: tokens.site_url
			site_path: tokens.site_path
			user_agent: tokens.user_agent
			site_origin: tokens.site_origin
			site_protocol: tokens.site_protocol

			product: tokens.product
			session_id: tokens.session_id
			form_language: tokens.formLanguage
			is_authenticated: tokens.isAuthenticated
			authentication_type: tokens.authenticationType
			transaction_reference: tokens.transactionReference

			form_uri: tokens.formUri
			publish_uri: tokens.publishUri
			section_length: tokens.sectionLength

			form_id: tokens.formId
			top_form_id: tokens.topFormId
			parent_form_id: tokens.parentFormId

			form_name: tokens.formName
			top_form_name: tokens.topFormName
			parent_form_name: tokens.parentFormName

			form_description: tokens.formDescription
			top_form_description: tokens.topFormDescription
			parent_form_description: tokens.parentFormDescription

			process_name: tokens.processName
			process_description: tokens.processDescription
			process_uri: tokens.processUri
			process_id: tokens.processId
			
			stage_id: tokens.stage_id
			stage_name: tokens.stage_name
			stage_length: tokens.stageLength

			user_id: tokens.user_id
			user_name: tokens.name
			user_fullname: tokens.name
			user_email: tokens.email
			user_ip: tokens.ipAddress
			user_lat: tokens.latitude
			user_lon: tokens.longitude
			user_lng: tokens.longitude
			user_city: tokens.cityName
			user_region: tokens.regionName
			user_country: tokens.countryName
			user_postcode: tokens.zipCode
			user_country_code: tokens.countryCode
			user_timezone: tokens.timeZone

			master_user_id: tokens.master_user_id
			master_user_name: tokens.master_user_name
			impersonated_user_id: tokens.impersonated_user_id
			impersonated_user_name: tokens.impersonated_user_name

			SaveURL: tokens.SaveURL
			save_url: tokens.SaveURL

			summary_ref: tokens.case_ref or tokens.reference

			# Task / Case Data

			db_id: tokens.db_id

			previous_db_id: tokens.previous_db_id

			case_id: tokens.case_ref

			task_id: tokens.task_ref

			previous_task_id: tokens.previous_task_ref

			case_ref: tokens.case_ref

			task_ref: tokens.task_ref

			previous_task_ref: tokens.previous_task_ref

			task_due: tokens.task_due
			
			case_due: tokens.case_due

			task_created: tokens.task_created

			case_created: tokens.case_created

			task_completed: tokens.task_completed

			case_completed: tokens.case_completed

			csa_id: tokens.csa_id

			csa_name: tokens.csa_name

			citizen_id: tokens.citizen_id

			citizen_name: tokens.citizen_name

			originator: tokens.originator

			originator_id: tokens.originator_id

			originator_name: tokens.originator_name

			view_group_id: tokens.view_group_id

			view_group_name: tokens.view_group_name

			assigned_user_id: tokens.assigned_user_id

			assigned_group_id: tokens.assigned_group_id

			assigned_user_name: tokens.assigned_user_name

			assigned_group_name: tokens.assigned_group_name

			published: tokens.published

			expired_status: tokens.expired_status

			escalation_status: tokens.escalation_status

			escalation_user_id: tokens.escalation_user_id

			escalation_group_id: tokens.escalation_group_id

			escalation_user_name: tokens.escalation_user_name

			escalation_group_name: tokens.escalation_group_name

			escalation_date: tokens.escalation_date

			escalation_warn_date: tokens.escalation_warn_date

	_fromForm: (name='summary', hidden, sub_name, sub_label, this_values=@options.values) ->
		val = []

		_.each this_values, (section) ~>
			_.each section, (field) ~>
				this_name = field.name?.toString?!
				this_type = field.type?.toString?!
				this_label = field.label?.toString?!

				return true if hidden is 1 and field._hidden
				return true if hidden is 2 and not field._hidden
					
				# return val.push @_fromSubform name, hidden, this_name, this_label, field.value if field.type is 'subform'
				
				this_val = ( field.value_label or field.value or '' )?.toString?!
				this_val = this_val?.replace? /\n/gmi, '<br/>' if this_type is 'textarea'
				this_val = @_buildTable(field) or 'No Summary Data' if this_type is 'subform'

				arr = []
				if name is 'fields'
					arr = ["<p>", this_val, "</p><br/>"]
				else if name is 'tokens'
					arr = ["<p>", this_name, "</p><br/>"]
				else if name is 'summary'
					arr = ["<p>", "<b>", this_label or this_name, ": </b>", "<span>", this_val, "</span>", "</p><br/>"]
		
				val.push arr.join ''
				return true
		
		val.join('') or "<p>#{if sub_name then "<b>#{sub_label}: </b>" else ''}No Summary Data</p>"

	_fromSubform: (name='summary', hidden, sub_name, sub_label, values) ->
		values = _.flatten [ values ]

		val = []; val_obj = {}
		_.each values, (this_values) ~>
			_.each this_values, (section) ~>
				_.each section, (field) ~>
					this_name = field.name?.toString?!
					arr = val_obj[this_name] ?= []

					return true if hidden is 1 and field._hidden
					return true if hidden is 2 and not field._hidden
					return val.push @_fromSubform name, hidden, this_name, this_label, field.value if field.type is 'subform'
					
					this_val = ( field.value_label or field.value or '')?.toString?!
					this_val = this_val?.replace? /\n/gmi, '<br/>' if field.type is 'textarea'

					if name is 'fields'
						arr.push this_val
					else if name is 'tokens'
						arr.push this_name
					else if name is 'summary'
						arr.push [ field.label or this_name, this_val ]					
					return true

		_.each val_obj, (vals, i) ->
			arr = []; if name is 'summary'
				_vals = _.map vals, (v) -> return v[1]
				arr.push ["<p>", "<b>" vals?[0]?[0]?.toString?!, ": </b>", "<span>", _vals?.join?(', '), "</span>", "</p><br/>"].join ''				
			else arr = ["<p>", vals?[0]?.toString?!, "</p><br/>"] 
			
			val.push arr.join ''
			return true

		val.join('') or "<p>#{if sub_name then "<b>#{sub_label}: </b>" else ''}No Summary Data</p>"

	_getStaticMap: (field) ->
		return field?.value_label or field?.value?.toString?! unless field?.value
		value_label = field.value_label
		[ lat, lng ] = field.value

		url = "https://maps.googleapis.com/maps/api/staticmap"
		
		query_string = querystring.stringify do
			center: "#{lat},#{lng}"
			key: settings.google.api_key
			zoom: settings.google.map_zoom
			size: settings.google.map_size
			scale: settings.google.map_scale
			maptype: settings.google.map_type
			markers: "color:red|#{lat},#{lng}"		

		return [ "<img src=\"#{url}?#{query_string}\" />", "<br/>", "<b>#{value_label or ''}</b>" ].join ''

	encode: (val, encoding) ->
		return val unless encoding = encoding?.toLowerCase?!

		switch encoding
			when 'xml' then utils.xmlEncode val, @_html_in_value, @_xml_in_value
			when 'url' then encodeURIComponent val
			when 'json' then val
			else val

	getValue: (token, isPrintable=false, encoding, values=@options.values) ->
		return '' unless token
		token = token?.replace? /[{}]/gmi, ''

		val = @getFieldValue(token, values, isPrintable) or
		@getEnvValue(token, isPrintable) or
		@getAFToken(token, isPrintable) or
		@getLocalToken(token, isPrintable) or
		@getGlobalValue(token, isPrintable) or ''

		return val unless val and encoding
		@_xml_in_value = true if isPrintable and token in settings.xml_token_types
		@_html_in_value = true if isPrintable and token in settings.html_token_types

		val = @encode val, encoding
		delete @_html_in_value
		delete @_xml_in_value
		return val

	getAFToken: (token) ->
		@af_tokens[token]

	getLocalToken: (token, isPrintable) ->
		return @options.tokens[token]

	getEnvValue: (token, isPrintable) ->
		@_html_in_value = true if isPrintable and @options.env_tokens[token]
		return @options.env_tokens[token]

	getGlobalValue: (token, isPrintable) ->
		return @options.global_tokens[token]

	set: (key, value) ->
		@options[key] = value

	setValues: (values) ->
		@options.values = values

	setFieldValue: (token, value, index, values) ->
		[ field, key ]  = @getField token, values

		if key then @_setInSubform field, key, value, index
		else field?.value = value

	_setInSubform: (field, key, value, index) ->
		set = (idx) ~>
			_val = if _.isUndefined idx then field.value else field.value[idx]
			_field = @getField(key, _val)?[0]
			_field?.value = value

		if index then set index
		else for own i, v of field.value then set i
		return field.value

	getField: (token, values=@options.values, isPrintable) ->
		unless token
			console.error 'Get Field No Token\n'
			console.trace(); return {}

		if token.split("/").length > 1
			[ dataname, key ] = token.split "/" 
		else dataname = token; key = null

		field = null; _.each values, (section) ->
			field := _.find section, (v, i) -> return v if i is dataname or v.name is dataname
			return false if field

		return [ field, key ]

	getFieldValue: (token, values=@options.values, isPrintable) ->

		[ field, key ] = @getField token, values, isPrintable

		console.info "\n['Tokenizer']", "Get Field Value: " token, key, field if @debug and not @test

		if key then @_findInSubform field, key, isPrintable 
		else @_finalizeValue field, isPrintable

	_findInSubform: (field, key, isPrintable) ->
		return '' if _.isEmpty field?.value
		value = _.flatten [ field.value ]
		arr = []; _.each value, (subform_entry) ~>
			try subform_entry = JSON.parse subform_entry
			arr.push @getFieldValue key, subform_entry, isPrintable
		return arr.join(', ')

	_finalizeValue: (field, isPrintable) ->
		return '' unless field?.value
		value = field.value?.replace?( /’/g, "'" )?.trim?! or 
				field.value?.trim?! or
				field.value

		return value unless isPrintable	
		@_html_in_value = true if field.type in settings.html_in_value_types

		switch 
			when field.type in settings.mapTypes and field.staticMap then value = @_getStaticMap field
			when field.type is 'textarea' then value = value?.replace? /\n/gmi, '<br/>'
			when field.type is 'subform' then value = value = @_buildTable field
			else value = field.value_label if field.value_label

		value = value?.toString?!
		return value

	hasTokens: (text) ->
		found = text.match /\{([^\}]+)\}/gmi
		try _found = JSON.parse found
		found and not _found

	getTokens: (text, arr=[]) ->
		if _.isObject text then for k, v of text
			arr = arr.concat @getTokens(v) or []
		else if _.isString text and @hasTokens text
			found = text.match(/\{([^\}]+)\}/gmi) or []
			found = _.map found, -> return it?.replace? /^{#/, '{'
			arr = arr.concat found if found
		return _.uniq arr

	replaceTokens: (text, isPrintable, encoding) ->
		return '' unless text

		if _.isBoolean text
			return text

		else if _.isString text
			return text?.replace? /\{([^\}]+)\}/gmi, (token, name) ~> 
				val = @getValue name?.replace?(/^#/, ''), isPrintable, encoding
				val = JSON.stringify val if _.isObject val
				return val or ''
		
		else if _.isArray text
			arr = []; for k, v of text
				arr.push @replaceTokens v, isPrintable, encoding
			return arr

		else if _.isObject text
			obj = {}; for k, v of text
				obj[k] = @replaceTokens v, (isPrintable unless k in ['attachments', 'pdf_attachments']), ( encoding if k in settings.encodingKeys )
			return obj

	parameterizeTokens: (text, encoding) ->
		params = []

		_getVal = (name) ~>			
			val = @getValue name, false, encoding
			val = val.join ', ' if _.isArray val
			val = JSON.stringify val if _.isObject val
			return val or ''

		query = text?.replace? /\{([^\}]+)\}/gmi, (token, name) ~>
			if name.charAt(0) is '#'
				new_name = name.substring(1)
				new_token = "{#{new_name}}"
				return _getVal new_name

			value = _getVal name
			IsNull = (if value then false else true).toString!					
			param_name = name?.split('/').join('_')  # + uuid.v4!.replace(/\-/gmi, "").substr(0, 6)	
			type = if value.match /^[0-9]+$/gmi and not ( value.charAt(0) in [ '0', 0 ] ) and parseFloat(value) < 2147483647 then 'Int' else 'String'
			params.push name: param_name, type: type, IsNull: IsNull, value: utils.xmlEncode value
			return ":#{param_name}"

		return [ query, params ]

	updateSubmissionValues: (data, type) ->
		values = @options.values
		return values unless type in settings.updateTypes

		obj = @_transform data
		return values unless rows = obj.rows_data

		_.each rows, (row) ->
			for own i, section of values
				for own i, field of section
					continue if _.isUndefined row[field.name]
					field.value = row[field.name]
					field.value_label = row[field.name] if field.value_label
		@options.values = values

	_buildTable: (field) ->

		_buildTotalsRow = (data) ->
			row = '<tr style="text-align:center">'	
			open_tag = '<td style="text-align:center;border-top:solid 1px #ccc">'; close_tag = '</td>'		
			for own x, y of data
				row += open_tag + y + close_tag
			row += '</tr>'

		_buildRow = (data, isHead) ->
			if isHead then (open_tag = '<th style="text-align:center;border-bottom: 1px solid #ccc;">'; close_tag = '</th>')
			else (open_tag = '<td style="text-align:center">'; close_tag = '</td>')

			row = if isHead then '<thead >' else ''
			row += '<tr style="text-align:center;">'
			for own x, y of data
				val = if isHead then y else y.value_label or y.value
				row += open_tag + val + close_tag
			row += '</tr>'
			row += if isHead then '</thead>' else ''
			return row

		_totals = field.totals
		return '' unless summ = field.summary
		summ = [ summ ] unless _.isArray summ 
		summ = [ summ ] unless field.isRepeatable
		return '' unless _.size summ?[0]?[1]

		table = '<table style="border: 1px solid #ccc; width:100%;">'

		if @debug and not @test
			try console.info "\n['Tokenizer']", "#{field.name} Summary\n", JSON.stringify summ

		for own x, summary of summ	
			table += _buildRow summary[1], true if summary?[1] and not parseInt x
			table += _buildRow summary[0] if summary?[0]		
		table += _buildTotalsRow _totals if _totals?.length
		table += '</table>'
		return table

	_transform: (data='', template={}) ->
		$data = $ data?.trim?!
		$error = $data.find("Error").html!
		$rows = $data.find("Rows").children!
		$fields = $data.find("Fields").children!
		$field_obj = {}; $row_obj = {}; $select_array = []

		getAttributes = (v, attributes={}) ->
			$.each v.get(0).attributes, (i, attrib) -> attributes[attrib.name] = attrib.value if attrib.name
			return attributes

		$fields?.each? (i, v) ~> 
			$v = $ v; attr = getAttributes $v
			$field_obj[attr.name] = attr if attr.name
			return true

		$rows?.each? (i, v) ~>
			$v = $ v; attr = getAttributes $v
			results = $v.children!			
			$this_row = {}
			id = attr.id

			results.each (x, y) ~>
				$y = $ y; col = $y.attr("column") or $y.text!
				$this_row[col] = utils.xmlDecode $y.text! if col
				return true

			key = $this_row["name"] or id
			$row_obj[key] = $this_row

			$obj = 
				label: utils.xmlDecode $this_row.display or $this_row.label
				value: utils.xmlDecode $this_row.name or $this_row.value or $this_row.display or $this_row.label
			$select_array.push $obj if _.size $obj.label and _.size $obj.value
			return true

		return ret =
			xml_data: data
			rows_data: $row_obj
			error: $error or null
			fields_data: $field_obj
			select_data: $select_array
			results_page: template.results_page
