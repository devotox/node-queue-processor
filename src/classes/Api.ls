
require! {
	_: \lodash
	Promise: \bluebird
	validUrl: \valid-url
	Validator: \validator
	Request: \request-promise
}

utils = require "./utils"

module.exports = class Api

	defaults = 	
		max_retries: 5
		customer_id: null
		app_name: \FS-Node
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'API Options\n', options

		@options = _.defaults options, defaults

		throw new Error \NoDocapiUrlError unless @options.docapi_url
		throw new Error \NoCustomerIdError unless @options.customer_id
		throw new Error \NoApibrokerUrlError unless @options.apibroker_url

		unless validUrl.isUri @options.docapi_url
			throw new Error "InvalidDocapiUrlError: #{@options.docapi_url}"

		unless validUrl.isUri @options.apibroker_url
			throw new Error "InvalidApibrokerUrlError: #{@options.apibroker_url}"

		@app_name = @options.app_name 
		@customer_id = @options.customer_id

		@docapi_url =  @options.docapi_url
		@broker_url = @options.apibroker_url

	apiUrl: (api) ->
		throw new Error \NoApiSelectedError unless api

		useSlash = (api.charAt 0) is "/"

		if useSlash then url = "#{@broker_url.replace /\/$/, ''}#{api}" 
		else url = "#{@broker_url}?api=#{api}" 
		return url

	docapi: (uri, op="get", args) ->		
		# Ensure we're in a promise chain in case of any errors
		Promise.resolve yes .bind @

		.then -> 
			throw new Error \NoUriInDocapiRequestError unless uri or op is "doclist"
			@docapi_url

		.then (url) -> @createRequest url, null, { uri: uri, op: op, args: args }

		.then (req) -> @send req, 'post'

		.then (body) -> @transform.response body

	get: (api, qs, base64) ->
		# Ensure we're in a promise chain in case of any errors
		Promise.resolve yes .bind @

		.then -> @apiUrl api

		.then (url) -> @createRequest url, qs

		.then (req) -> @send req, 'get'

		.then (body) -> @transform.response body, base64


	post: (api, qs, args, base64) ->
		# Ensure we're in a promise chain in case of any errors
		Promise.resolve yes .bind @

		.then -> @apiUrl api

		.then (url) -> @createRequest url, qs, args

		.then (req) -> @send req, 'post'

		.then (body) -> @transform.response body, base64


	put: (api, qs, args, base64) ->
		# Ensure we're in a promise chain in case of any errors
		Promise.resolve yes .bind @

		.then -> @apiUrl api

		.then (url) -> @createRequest url, qs, args

		.then (req) -> @send req, 'put'

		.then (body) -> @transform.response body, base64

	send: (req, type="get", retries=0) ->
		req.method = type.toUpperCase()
		
		console.info "['API'] ['#{req.method}'] Request URL:", req.uri if @debug and not @test
		
		Promise.resolve yes .bind @
		
		.then -> Request req

		.then (body) -> 
			try body = JSON.parse body
			if @debug and not @test and not body?.content		
				try 	
					_req = _.merge {}, _.clone req
					_req.body = JSON.parse _req.body
					_req?.body?.args?.metadata = JSON.stringify _req?.body?.args?.metadata
					_req?.body?.args?.content = "Emptied for console debugging, Content Length: #{_req?.body?.args?.content.length}" if _req

				console.info "\n['API'] ['#{req.method}'] Request\n", _req or req
				console.info "\n\n['API'] ['#{req.method}'] Response\n", body, '\n\n'

			return body

		.caught -> 
			err = if _.isObject it?.error then JSON.stringify it?.error else it?.error
			err ?= if _.isObject it?.message then JSON.stringify it?.message else it?.message or it
			console.info "['API'] ['#{req.method}'] Request URL: #{req.uri} || Error: #{err}" if @debug and not @test
			
			retries := parseInt(retries) or 0
			throw new Error err if @options.isTest or retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @send req, type, retries

	createRequest: (uri, qs={}, args={}) ->
		throw new Error \NoUriInCreateRequestError unless uri
		_.extend qs, do
			app_name: @app_name
			customer_id: @customer_id

		body = JSON.stringify args
		req = qs: qs, uri: uri, body: body

	transform:
		isError: (txt) ->
			return unless txt
			isErr = txt instanceof Error or txt?.Error		
			isErr or= txt?.indexOf?(\<Error>) > -1 if _.isString txt
			isErr

		response: (body, base64=true) ->
			return unless body

			try body = JSON.parse body

			if @isError (body?.content or body)
				throw new Error (body?.Error or body?.message or body?.content or body)
				
			if body?.content
				body.content = utils.fromBase64 body.content if base64
				try body.content = JSON.parse body.content

			if body?.metadata
				body.metadata = @metadata body.metadata

			return body

		metadata: (meta) ->	
			return unless meta

			obj = {}; for i, v of meta
				v.Value = '' if _.isEmpty v.Value
				obj[v.Name] = v.Value
			return obj

