
require! {
	_: \lodash
	Promise: \bluebird
}

API = require "./Api"
utils = require "./utils"
Integration = require "./Integration"
Notification = require "./Notification"

module.exports = class Escalation
	settings =
		open: 1
		closed: 0

		escalated: 1
		unescalated: 0
		no_escalation: -1
		
	defaults = 
		customer_id: null
		publish_endpoint: "sandbox-publish://"
		process_endpoint: "sandbox-processes://"
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	class NoTasks extends Error 
	class NoEscalation extends Error

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Escalation Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoDocapiUrlError unless @options.docapi_url
		throw new Error \NoCustomerIdError unless @options.customer_id
		throw new Error \NoApibrokerUrlError unless @options.apibroker_url

		options.customer_id = \eeda5230-55b2-4b74-b2f0-d4fd37711bba
		@_escalated_tasks = []
		@created_date = new Date! .toString "yyyy-MM-dd HH:mm:ss"

		@api = new API @options, @debug, @test

	NoTasks: NoTasks

	NoEscalation: NoEscalation 

	run: ->
		Promise.resolve true .bind @

		.then -> @getTasks!

		.then -> @escalate!

		.then -> @_escalated_tasks

		.caught @NoTasks, -> []

		.caught -> throw new Error "Escalate Run: #{it.message or it}" 

	escalate: (tasks) ->
		tasks = _.flatten [ tasks ]

		Promise.all tasks.map @_escalate.bind @ .bind @

		.then -> @_escalated_cases = it

	_escalate: (task) ->

		stage = null

		process = null

		@getProcess task .bind @

		.then -> process := it

		.then -> @checkEscalation task, process

		.then -> stage := it

		.then -> @updateTask task, stage

		.then -> @updateRoutes task, stage, process

		.then -> @notify stage

		.caught @NoEscalation, _.identity

		.then -> @getCase task.case_id

		.caught -> throw new Error "Escalate Error: [#{stage.name}] #{it.message or it}"

	getCase: (case_id) ->
		@api.get "/listCases", case_id: case_id

	updateTask: (task, stage, data) ->

		@api.post "/updateTask", null, @updateTaskArgs task, data, stage .bind @

		.then (data) ->  @checkResponse data

		.then -> @_escalated_tasks?.push task

		.caught (err) -> throw new Error "Update Task Error: #{err.message or err}"

	updateRoutes: (task, stage, data) ->


	checkResponse: (data={}) ->
		throw new Error data.error if data.error
		throw new Error data.message if data.success is false
		return data

	checkEscalation: (task, process) ->

		stage = _.find process.stages, (v, i) ~>
			return v if v.id is task.stage_id or v.name is task.stage_name

		escalation = stage?.props?.escalation

		Promise.reject \NoEscalation unless _.size escalation

		Promise.resolve stage

	getProcess: (task) ->
		uri = @options.endpoint + task.process_id

		@api.docapi uri .bind @ 

		.then -> it?.content or it

		.caught -> throw new Error "Get Process Error: #{it.message or it}"

	getTasks: ->

		@api.get "/listTasks", @getEscalateArgs! .bind @

		.caught -> throw new Error "Get Tasks Error: #{it.message or it}"

		.then (tasks) ->
			return tasks if tasks?.length
			throw new @NoTasks 'No Tasks To Escalate'

	nextTask: (stage, process) ->
		tasks = []
		_.each stage.routes, (route, i) ~>
			return true unless next_stage = process.stages[i]
			tasks.push @createTask next_stage, process
			return true	

		Promise.all tasks

		.caught (err) -> throw new Error "Next Task - #{err.message or err}"

	createTask: (stage, process, data) ->

		@api.post "/createTask", null, @createTaskArgs stage, process, data .bind @

		.then (data) -> @checkResponse data

		.then (data) -> @notify stage, settings.open

		.then (data) -> @integrations stage, settings.open

		.caught -> throw new Error "Create Task Error: #{it.message or it}"

	notify: (stage=@stage, notification) ->
		return unless notification or= stage?.props?.escalation?.notification
		return unless notification.recipients_to or notification.notify_assigned_users

		notification.recipients_to or= ''
		notification.subject or= "#{stage.name} Escalated"
		notification.body or= "New Task #{stage.name} Escalated"

		if notification.notify_assigned_users in [ true, 'true' ]
			n = notification.recipients_to.split(',')
			n.push stage.props.user
			notification.recipients_to = n.join ','

		recipients = _.map notification.recipients_to.split(','), (email) -> return [ '', email?.trim?(), 'To' ]
		if recipients?.length then recipients = _.filter recipients, (rec) -> return rec?[1] and rec?[2]
		return unless recipients?.length

		opts = 
			API: 
				docapi_url: @options.docapi_url
				customer_id: @options.customer_id
				apibroker_url: @options.apibroker_url

			template: 
				html: true, type: \email, recipients: recipients
				from: [ \notifications@firmstep.com, \Firmstep ]
				subject: notification.subject, message: notification.body

		opts.LIM = id: notification?.LIM_id if notification.LIM_id
		new Notification opts .send!

	updateTaskArgs: (task, data={}) ->
		args =
			id: task.id
			escalation_status: settings.escalated
			assigned_user_id: task.escalation_user_id
			assigned_group_id: task.escalation_group_id
		_.extend args, data

	getEscalateArgs: (data={}) ->
		args =
			case_status: settings.open #exclude closed cases 
			task_status: settings.open #exclude closed tasks
			escalation_status: settings.unescalated #include status not yet escalated
			max_date_escalation: "Now()" #exclude everything that expires later than now
		_.extend args, data
	
	createTaskArgs: (stage, process, submission, data={}) ->
		args = 
			data: null
			task_id: null

			case_id: @case_id
			task_status: settings.open
			case_status: settings.open
			
			process_id: process.props.id
			process_name: process.processName
			view_group_id: process.props.viewGroup

			stage_id: stage.id 
			stage_name: stage.name
			stage_form: stage.props.form
			assigned_user_id: @getUser stage
			assigned_group_id: @getGroup stage

			date_completed: null
			date_created: @created_date
			date_started: @created_date

			escalation_user_id: @getUser stage, true
			escalation_group_id: @getGroup stage, true
			escalation_status: if stage.props?.escalation?.date then 0 else -1
			date_escalation: Date.parse stage.props?.escalation?.date, "yyyy-mm-dd HH:mm:ss"

			published: if submission.isPublished then 1 else 0
			createCase: unless @case_id then settings.open else null
			citizen_id: submission.citizen_id or submission.user?.user_id
			citizen_name: submission.citizen_name or submission.user?.name

		_.extend args, data
		
	getUser: (stage, escalation) ->
		unless escalation
			user = @tokenizer.getValue stage.props.user_token
			user = null unless validator.validate user
			user or= stage.props.user or ''
		else 
			user = @tokenizer.getValue stage.props.escalation?.user_token
			user = null unless validator.validate user
			user or= stage.props.escalation?.user or ''
		return user

	getGroup: (stage, escalation) ->
		unless escalation
			group = @tokenizer.getValue stage.props.group_token
			group or= stage.props.group or ''
		else 
			group = @tokenizer.getValue stage.props.escalation?.group_token
			group or= stage.props.escalation?.group or ''
		return group


/*
	routes = (taskData, processDef) ->
		routingPromise = Q.defer()
				
		return routingPromise.reject() unless taskData?.stage_name and processDef?.stages?[taskData.stage_name]?.routes
		routes = processDef.stages[taskData.stage_name].routes
		routesStarted = 0
		createStageRequests = []
		for own routeName, route of routes
			continue unless route.onEscalation
			
			#evaluate condition
			# TODO do something with conditions
			doEscalation = true

			if doEscalation
				next_stage = processDef.stages[route.destination]
				continue unless next_stage
				next_task_args =
					case_id: taskData.case_id
					stage_id: next_stage.id
					stage_name: route.destination
					stage_form: next_stage.props?.form
					assigned_user_id: next_stage.props?.user #assign to normal user, not escalation user (that's for when the NEXT stage escalates)
					assigned_group_id: next_stage.props?.group # as per user
					data: taskData.data #pass data through
					task_status: "1" #open
					date_escalation: next_stage.props?.escalation?.date

				createStageRequests.push @apibroker("/createTask", "post", null, next_task_args).then (data) =>
					@error data.error if data?.error
					routesStarted++

		Q.all(createStageRequests).spread ->
			routingPromise.resolve(routesStarted)

		routingPromise.promise */
