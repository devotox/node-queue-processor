require! {
	fs: \fs
	_: \lodash
	knex: \knex
	bugsnag: \bugsnag
	Promise: \bluebird
	libxmljs: \libxmljs
}

docroot = "../../../../"

API = require "./Api"
require "./../libs/date"
utils = require "./utils"
Integration = require "./Integration"
Notification = require "./Notification"
LimConnect = require "#{docroot}FS-LIM-Connect/build/src/app"

module.exports = class DataDump

	settings = 
		col_length: 200
		col_type: \string
		name_regex: /[^a-zA-Z0-9\_]/g
		data_types: [ 'tasks', 'cases', 'submissions' ]
		default_query_opts:
			system: name: "Data Dump System", id: "53550c447bb5f"
			lim: name: "Data Dump LIM", id: "3f07ecf4-0dfa-11e2-b0b9-12313b03ed59"

	defaults = 
		convert: no
		query_opts: {}
		client: \mysql
		print_data: yes
		connection: null
		customer_id: null
		published_only: no
		notify: \devonte.emokpae@firmstep.com
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Data Dump Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoDocapiUrlError unless @options.docapi_url
		throw new Error \NoCustomerIdError unless @options.customer_id
		throw new Error \NoApibrokerUrlError unless @options.apibroker_url

		@columns = {}; @all_table_names = []; @errors = [];
		@current_table_columns = {}; @current_table_column_diff = {};

		@api = new API @options, @debug, @test

		@knex = knex do
			debug: true
			connection: @options.connection
			client: @add!.client @options.client

##======================================== RUN DATA DUMP ============================================================##
	writeFile: (name, file) ->
		fs.writeFileSync name, file

	run: ->
		Promise.resolve true .bind @

		.then @prepareAll

		.then -> 
			return unless @options.print_data
			console.info "['Data Dump'] writing to file: create and insert queries..."
			@writeFile "create.datadump.json", JSON.stringify [ _.sortBy(@all_table_names, -> it.toLowerCase!) , [ @tasks_table_create_sql, @cases_table_create_sql, @submissions_table_create_sql ] ], null, 4
			@writeFile "insert-delete-alter.datadump.json", JSON.stringify [ @all_table_alter_sql, [ @tasks_table_insert_sql, @cases_table_insert_sql, @submissions_table_insert_sql ] ], null, 4

		.then @runAlter

		.then @runCreate

		.then @runDelete

		.then @runInsert

		.then -> 
			return unless @options.print_data
			console.info "['Data Dump'] writing to file: errors..."
			@writeFile "errors.datadump.json", JSON.stringify @errors, null, 4

		.then -> [ @options.customer_id, @options.client ]

		.caught -> throw new Error (it.message or it)

##======================================== RUN DB INTEGRATION (THROUGH LIM) ============================================================##

	runSQL: (query, query_opts=@options.query_opts) ->

		if _.isObject query 
			tasks = _.map query, ~> @runSQL it
			return Promise.all tasks

		default_query_opts = settings.default_query_opts
		_.extend query_opts, default_query_opts if @test

		query_opts.lim?.name ?= default_query_opts.lim.name
		query_opts.system?.name ?= default_query_opts.system.name

		query_opts.original_query = query_opts.query = query
		query = query_opts.query = @sql!.convert query if @options.convert

		id = "Data-Dump-#{Date.today().toString('yyyy-MM-dd')}"
		name = "Data Dump #{Date.today().toString('dd/MM/yyyy')} - #{query_opts?.query?.substring?(0, 50)}"

		Output_template = 
			query: query_opts.query
			systemID: query_opts.system.id
			systemName: query_opts.system.name

		template = 
			ID: id
			Name: name
			Editable: false
			isLookup: false
			Type: 'DATABASE'
			integration: id: id
			allow_offline: false
			Category: 'Data Dump'
			data_type: 'integration'
			log_only_on_error: true
			error_notification: null
			LIM_id: query_opts.lim.id
			LIM_name: query_opts.lim.name
			Output_template: JSON.stringify Output_template

		opts =
			noLog: true
			throwError: yes
			template: template
			customer_id: @options.customer_id
			global_tokens: { global_token1: \fake-token }
			submission: { formValues: { Section1: {} }, env_tokens: {}, tokens: { token1: \fake-token } }

		@runIntegration opts, query_opts

	runIntegration: (opts, query_opts) ->
		opts = _.merge {}, _.cloneDeep opts
		query_opts = _.merge {}, _.cloneDeep query_opts

		new Integration opts, @debug, @test .run! .bind @

		.caught -> @notify it, query_opts, opts

	notify: (err, query_opts, opts) ->
		return if err?.message?.indexOf('already exists') > -1
		return if err?.message?.indexOf('There is already an object named') > -1		
		console.error '\n', "['Data Dump']", 'QUERY:', query_opts?.query, '\n\n', '\nERROR\n', err, '\n'
		
		@errors.push do
			query: query_opts?.query
			query_opts: query_opts
			opts: opts, error: err

##======================================== PREPARE ============================================================##

	prepareAll: ->
		Promise.all [ @prepareTasks!, @prepareCases!, @prepareSubmissions! ] 

		.bind @ .then @prepareAlter

	prepareCases: ->
		console.info "['Data Dump'] Preparing Cases..."

		Promise.resolve true .bind @

		.then -> @getCases!

		.then -> @cases = it

		.then -> console.info "['Data Dump'] Cases Retrieved..."

		.then -> @createTable!.caseSql @cases

		.then -> @cases_table_create_sql = it

		.then -> console.info "['Data Dump'] Cases Create Table SQL created..."

		.then -> @insertTable!.caseSql @cases

		.then -> @cases_table_insert_sql = it

		.then -> console.info "['Data Dump'] Cases Insert Table SQL created..."

		.then -> console.info "['Data Dump'] Cases Prepared..."

	prepareTasks: ->
		console.info "['Data Dump'] Preparing Tasks..."

		Promise.resolve true .bind @

		.then -> @getTasks!

		.then -> @tasks = it

		.then -> console.info "['Data Dump'] Tasks Retrieved..."

		.then -> @createTable!.taskSql @tasks

		.then -> @tasks_table_create_sql = it

		.then -> console.info "['Data Dump'] Tasks Create Table SQL created..."

		.then -> @insertTable!.taskSql @tasks

		.then -> @tasks_table_insert_sql = it

		.then -> console.info "['Data Dump'] Tasks Insert Table SQL created..."

		.then -> console.info "['Data Dump'] Tasks Prepared..."

	prepareSubmissions: ->
		console.info "['Data Dump'] Preparing Submissions..."

		Promise.resolve true .bind @

		.then -> @getSubmissions!

		.then -> @submissions = it

		.then -> console.info "['Data Dump'] Submissions Retrieved..."

		.then -> @createTable!.submissionSql @submissions

		.then -> @submissions_table_create_sql = it

		.then -> console.info "['Data Dump'] Submissions Create Table SQL created..."

		.then -> @insertTable!.submissionSql @submissions

		.then -> @submissions_table_insert_sql = it

		.then -> console.info "['Data Dump'] Submissions Insert Table SQL created..."

		.then -> console.info "['Data Dump'] Submissions Prepared..."

	prepareAlter: ->
		console.info "['Data Dump'] Preparing Alter..."

		Promise.resolve true .bind @

		.then -> @checkExists!

		.then -> @alterTable!.allSql!

		.then -> @all_table_alter_sql = it

		.then -> console.info "['Data Dump'] Alter Prepared..."

##======================================== RUN ============================================================##

	runAlter: ->
		@runSQL [ @all_table_alter_sql ] if _.size @all_table_alter_sql

	runCreate: ->
		@runSQL [ @tasks_table_create_sql, @cases_table_create_sql, @submissions_table_create_sql ]

	runDelete: ->
		arr = [ @tasks_table_insert_sql, @cases_table_insert_sql, @submissions_table_insert_sql ]
		new_arr = _.flatten _.map arr, (sqls) -> _.map sqls, (sql) -> sql.del
		@all_table_delete_sql = new_arr
		@runSQL new_arr

	runInsert: ->
		arr = [ @tasks_table_insert_sql, @cases_table_insert_sql, @submissions_table_insert_sql ]
		new_arr = _.flatten _.map arr, (sqls) -> _.map sqls, (sql) -> sql.ins
		@all_table_insert_sql = new_arr
		@runSQL new_arr

##======================================== GET ============================================================##

	getDocument: (uri) -> 
		@api.docapi uri

	getTasks: (params) ->
		@api.get "/listTasks", params

	getCases: (params) ->
		@api.get "/listCases", params

	getSubmissions: ->
		@api.get "ListSubmissions"

		.then (data) ->
			new_data = []; _.each data, (v, i) ->
				new_att = 'submission-uri': v.Name; 
				_.each v.Attribute, (y) -> new_att[y.Name] = if _.size y.Value then y.Value else ''
				new_data.push new_att
			return new_data

##======================================== CREATE ============================================================##

	createTable: -> return
		taskSql: (tasks) ~>
			sqls = {}; columns = {}
			_.each tasks, (_task) ~>
				_task.process_name = that.task.process_name if columns[_task.process_id]

				columns[_task.process_id] ?=
					task: _task
					table_id: null
					table_name: null
					column_names: [
						{ name: "ID" }
						{ name: "Status" }
						{ name: "Reference" }
						{ name: "created_date" }
						{ name: "completed_date" }
					]

			@columns.tasks = columns
			_.each columns, (column) ~>
				_task = column.task
				return true unless _task.process_name
				column.column_names = @add!.cleanup column.column_names
				column.process_name = _task.process_name.replace settings.name_regex, '_'

				column.table_id = _task.process_id
				column.table_name = "s_#{column.process_name}"
				return @add!.joinColumns column.table_name, column, columns, sqls if column.table_name in @all_table_names

				@all_table_names.push column.table_name
				sqls[column.table_id] = @sql!.createTable column.table_name, column.column_names
			return Promise.resolve sqls

		caseSql: (cases) ~>
			sqls = {}; columns = {}
			_.each cases, (_case) ~>
				_case.process_name = that.case.process_name if columns[_case.process_id]

				columns[_case.process_id] ?=
					case: _case
					table_id: null
					table_name: null
					column_names: [ 
						{ name: "ID" }
						{ name: "Status" }
						{ name: "Reference" }
						{ name: "created_date" }
						{ name: "completed_date" }
					]
				@add!.columns _case.aggregate_data, columns[_case.process_id].column_names

			@columns.cases = columns
			_.each columns, (column) ~>
				_case = column.case
				return true unless _case.process_name
				column.column_names = @add!.cleanup column.column_names
				column.process_name = _case.process_name.replace settings.name_regex, '_'
				
				column.table_id = _case.process_id
				column.table_name = "p_#{column.process_name}"
				return @add!.joinColumns column.table_name, column, columns, sqls if column.table_name in @all_table_names

				@all_table_names.push column.table_name
				sqls[column.table_id] = @sql!.createTable column.table_name, column.column_names
			return Promise.resolve sqls

		submissionSql: (submissions) ~>
			sqls = {}; columns = {}
			_.each submissions, (_sub, i) ~>
				_sub.form_id = _sub['form-uri']?.split('//')[1]
				return true unless _sub.form_id and _sub['submission-uri'] and _sub.form_id isnt 'undefined'
				_sub._form_uri = _sub['publish-form-uri'] or _sub['publish-uri'] or _sub['form-uri']
				_sub['form-name'] = that.sub['form-name'] if columns[_sub.form_id]
					
				columns[_sub.form_id] ?=
					sub: _sub
					table_id: null
					table_name: null
					column_names: [ 
						{ name: "ID" }
						{ name: "Status" }
						{ name: "Reference" }
						{ name: "created_date" }
						{ name: "completed_date" }
					]

			tasks = []; _.each columns, (column) ~>
				tasks.push @add!.submissionFields column, sqls	

			ret = Promise.defer()
			Promise.all tasks .bind @ .lastly -> 
				@columns.submissions = columns
				_.each columns, (column) ~>
					_sub = column.sub
					return true unless _sub.form_name
					return true if _sub.form_id?.indexOf('AF-Process') > -1
					column.column_names = @add!.cleanup column.column_names
					column.form_name = _sub.form_name.replace settings.name_regex, '_'
					
					column.table_id = _sub.form_id
					column.table_name = "f_#{column.form_name}"
					return @add!.joinColumns column.table_name, column, columns, sqls if column.table_name in @all_table_names

					@all_table_names.push column.table_name
					sqls[column.table_id] = @sql!.createTable column.table_name, column.column_names
				ret.resolve sqls
			ret.promise

##======================================== INSERT ============================================================##

	insertTable: -> return		
		taskSql: (tasks) ~>
			rows = []; _.each tasks, (_task={}) ~>
				id = _task.id
				ref = _task.case_id
				_task_column = @columns.tasks[_task.process_id]
				table = _task_column.table_name;

				row_obj =
					ID: id, Reference: ref, Status: @add!.status _task.task_status
					created_date: _task.date_started?.split('T').join(' ').split('+')[0]
					completed_date: _task.date_completed?.split('T').join(' ').split('+')[0]
				rows.push ins: @sql!.insertRow(table, row_obj), del: @sql!.deleteRow table, id, ref
			return rows

		caseSql: (cases) ~>
			sqls = {}; rows = []
			_.each cases, (_case) ~>
				id = _case.case_id
				ref = _case.case_id
				_case_column = @columns.cases[_case.process_id]
				table = _case_column.table_name;

				row_obj = 
					ID: id, Reference: ref, Status: @add!.status _case.case_status
					created_date: _case.date_started?.split('T').join(' ').split('+')[0] or ''
					completed_date: _case.date_completed?.split('T').join(' ').split('+')[0] or ''
				
				@add!.rows _case.aggregate_data, row_obj, @columns.cases[_case.process_id].column_names
				rows.push ins: @sql!.insertRow(table, row_obj), del: @sql!.deleteRow table, id, ref
			return rows

		submissionSql: (submissions) ~>
			sqls = {}; rows = []
			_.each submissions, (_sub) ~>
				return true if _sub.form_id?.indexOf('AF-Process') > -1
				return true unless _sub.form_name and @columns.submissions[_sub.form_id]

				ref = _sub.id
				id = _sub.filename
				_sub_column = @columns.submissions[_sub.form_id]
				table = _sub_column.table_name

				row_obj = 
					ID: id, Reference: ref, Status: 'closed'
					created_date: _sub.last_modified?.split('T').join(' ').split('+')[0] or ''
					completed_date: _sub.last_modified?.split('T').join(' ').split('+')[0] or ''

				@add!.rows _sub.formValues, row_obj, @columns.submissions[_sub.form_id].column_names
				rows.push ins: @sql!.insertRow(table, row_obj), del: @sql!.deleteRow table, id, ref
			return rows

##======================================== ALTER ============================================================##

	alterTable: -> return
		allSql: ~>
			_.map @current_table_column_diff, (columns, table_name) ~>
				return '' unless columns?.length and table_name
				@sql!.alterTable table_name, columns

##======================================== SQL Query Builder (Knex) ============================================================##

	sql: -> return
		convert: (query) -> # Converts MySQL to MSSQL
			return query unless query; f = true

			query = query.replace /(unsigned|auto_increment)/g, (m) ->
				return "check ([ID] > 0)" if m is 'unsigned'
				return "identity" if m is 'auto_increment'

			# replace all alter add columns to only have add on first entry
			query = query.replace /add \`([^\`]+)\`/g, (m, a) -> 
				( f := false; return "add [#{a}]" ) if f; "[#{a}]"

			# replace back ticks with []
			query = query.replace /\`([^\`]+)\`/g, (m, a) -> "[#{a}]"

			# Replace escape characters
			query = query.replace /(\\\')+/g, (m, a) -> "''"
			query = query.replace /(\\\\)+/g, (m, a) -> "\\"

		deleteTable: (name, ifExists) ~>
			fn = if ifExists then 'dropTableIfExists' else 'dropTable'
			@knex.schema[fn] name .toString!

		renameTable: (name, new_name) ~>
			@knex.schema.renameTable name, new_name .toString!

		createTable: (name, columns=[], inc) ~>
			@knex.schema.createTable name, (table) ->
				table.increments inc if inc
				_.each columns, (v) ->
					return true unless v?.name
					v.type or= settings.col_type
					v.col_length or= settings.col_length
					table[v.type] v.name, v.col_length
			.toString!

		alterTable: (name, columns=[]) ~>
			@knex.schema.table name, (table) ->
				_.each columns, (v) ->
					return true unless v?.name
					v.type or= settings.col_type
					v.col_length or= settings.col_length
					return table.dropColumn v.name if v.drop
					table[v.type] v.name, v.col_length
			.toString!

		deleteRow: (table, id, ref) ~>
			return unless table and id and ref
			@knex(table)
				.where('ID', id)
				.orWhere -> @where ID: null, Reference: ref
				.del!.toString!

		insertRow: (table, ins_obj) ~>
			return unless table and _.isObject ins_obj
			@knex(table)
				.insert(ins_obj)
				.toString!

##======================================== UTILITIES ============================================================##

	add: -> return
		cleanup: (columns) ->
			_.uniq columns, -> it.name?.toLowerCase!

		status: (st) ->
			if st?.toString() is '1' then 'open' else 'closed'

		client: (c) ~>
			return c unless c = c?.toLowerCase!			
			( c = 'mysql'; @options.convert = true ) if c is 'mssql' 
			return c

		joinColumns: (table_name, column, columns, sqls) ~>
			return unless prev_column = _.find columns, table_name: table_name

			_.each column.column_names, (col) ~>
				return true if _.find prev_column.column_names, name: col?.name
				prev_column.column_names.push col

			sqls[prev_column.table_id] = @sql!.createTable prev_column.table_name, prev_column.column_names

		rows: (sub_data, rows, columns, prefix) ~>
			try sub_data = JSON.parse sub_data if _.isString sub_data
			return unless _.isObject sub_data

			_.each columns, (v) ~>
				return true if rows[v.name] or rows[v.name] in [ '', false ]

				_.each sub_data, (section) ~>
					return true unless _.size section

					_.each section, (field) ~>
						return true unless _name = field?.name?.replace settings.name_regex, '_'
						return @add!.rows field.value, rows, columns, _name if field.type is 'subform' and not field.isRepeatable
						return true if field.isRepeatable

						return true unless v.name is (row_name = if prefix then "#{prefix}_#{_name}" else _name)

						value = field.value or ''
						value = JSON.stringify value if _.isObject value
						rows[row_name] = "#{value}"?.substr? 0, parseInt(settings.col_length / 2)
						return false
					return false if rows[v.name]

		columns: (sub_data, columns, prefix) ~>
			try sub_data = JSON.parse sub_data if _.isString sub_data
			return unless _.isObject sub_data

			_.each sub_data, (section) ~>
				return true unless _.size section

				_.each section, (field) ~>
					return true unless _name = field?.name?.replace settings.name_regex, '_'
					return @add!.columns field.value, columns, _name if field.type is 'subform' and not field.isRepeatable
					return true if field.isRepeatable
					
					col_name = if prefix then "#{prefix}_#{_name}" else _name

					columns.push { name: col_name } unless _.find columns, { name: col_name }
					return true
		
		submissionFields: (column, sqls) ~>
			_sub = column.sub
			form_id = _sub['form_id']
			form_name = _sub['form-name']?.replace /[^a-zA-Z0-9\_]/g, '_'
			
			# return promise.resolve true unless _sub['submission-uri']

			@getDocument _sub['submission-uri'] .bind @ .then -> 
				return true if form_id?.indexOf('AF-Process') > -1 or not _.size it?.metadata

				_sub.formValues = it.content.formValues
				_sub.form_id = it.content.formId or form_id
				_sub.form_name = it.content.formName?.replace(settings.name_regex, '_') or form_name
				@add!.columns _sub.formValues, column.column_names

			.caught -> @errors.push sub_uri: _sub['submission-uri'], column_sub: _sub, column: column
		
##======================================== CHECKS ============================================================##

	exists: (table) -> 

		@runSQL "select count(*) has_table from information_schema.tables where table_name = '#{table}' "
		
		.then -> if _.isObject(it) and it?[0]?['has_table'] is '1' then true else false

	checkExists: ->
		tasks = []; _.each settings.data_types, (a) ~>
			_.each @columns[a], (v) ~>
				return true unless v.table_name and v.table_name isnt 'undefined'
				return true if v.sub?.form_id?.indexOf('AF-Process') > -1
				tasks.push @_checkExists v.table_name, v.table_id, v.column_names, @columns[a]
		Promise.all tasks

	_checkExists: (table_name, table_id, column_names, columns) ->
		Promise.resolve true .bind @

		.then -> @exists table_name

		.then -> @_setCurrentTableColumns table_name if it

		.then -> @getTableDiff column_names, it if _.size it

		.then -> @current_table_column_diff[table_name] = it if _.size it

	_setCurrentTableColumns: (table_name, table_id) ->

		Promise.resolve true .bind @
		
		.then -> @getCurrentTableColumns table_name

		.then -> @current_table_columns[table_name] = it;

	getTableDiff: (column_names=[], cur_column_names=[]) ->
		diff = _.cloneDeep column_names
		_.remove diff, (x) -> !! _.find cur_column_names, name : x.name
		return diff

	getCurrentTableColumns: (table_name) -> 

		@runSQL "select column_name from information_schema.columns where table_name = '#{table_name}' "

		.then @transformCurrentTableColumns

	transformCurrentTableColumns: (data) ->
		_.map data, (v) -> name: v.column_name, type: settings.col_type, col_length: settings.col_length

