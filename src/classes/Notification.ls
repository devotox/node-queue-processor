
require! {
	_: \lodash
	events: \events
}

root = "../"
docroot = "../../../../"

API = require "./Api"
require "#{root}libs/date"
LimConnect = require "#{docroot}FS-LIM-Connect/build/src/app"

module.exports = class Notification

	defaults = 
		LIM: 
			iv: null
			key: null
			url: null
			id: "3f07ecf4-0dfa-11e2-b0b9-12313b03ed59"

		API: 
			customer_id: null
			docapi_url: "http://localhost/docapi/"
			apibroker_url: "http://localhost/apibroker/"

		template: 
			priority: null
			attachments: null
			subject: "Firmstep Notification"
			message: "!!!Firmstep Notification!!!"
			html: true, type: \email, recipients: []
			from: [ \notifications@firmstep.com, \Firmstep ]

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Notification Options \n', options

		@options = _.defaults options, defaults

		if @options.LIM.id then @api = new API @options.API, @debug, @test
		else @limConnect = new LimConnect @options.LIM, @debug, @test

	send: (recipients, template) ->

		unless @options.LIM.id then @default recipients, template
		
		else @getLIM! .bind @ .then -> @default recipients, template

	getLIM: ->

		@api.get "/lims", { action: 'get', id: @options.LIM.id } .bind @

		.then -> it?[0] or it

		.then -> @options.LIM = it

		.then -> @limConnect = new LimConnect @options.LIM, @debug, @test

		.caught -> throw new Error "No LIM with ID: #{@options.LIM.id or @options.LIM.ID}"

	default: (recipients=[], template={}) ->

		recipients = [ ['', recipients, 'To'] ] if _.isString recipients
		recipients = @options.template.recipients if _.isEmpty recipients

		temp = _.extend @options.template, template
		body = { message: @options.template.message, html: (!!temp.html)?.toString! }
		
		throw new Error \NoMessageError unless body.message
		throw new Error \NoRecipientError unless recipients?.length
		throw new Error \NoSenderError unless temp.from?.length and _.isArray temp.from

		@limConnect.email recipients, temp.from, temp.subject, body, temp.attachments, temp.priority
		
		.bind @ .then -> it?.match(/<ResponseStatus[^>]*>(.*?)<\/ResponseStatus>/)?[1]?.toString?!

		.caught -> throw new Error (it?.message or it)

