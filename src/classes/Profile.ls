
require! {
	fs: \fs
	_: \lodash
	Promise: \bluebird
	param: \node-jquery-param
}

API = require "./Api"
require "./../libs/date"
utils = require "./utils"

module.exports = class Profile
	settings = 
		get_api: "GetCustomerByUCRN"
		update_api: "UpdateCustomer-system"
		create_api: "createcustomer-userchain-system"
		allowed_actions: <[ get create update ]>

	defaults = 
		customer_id: null
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Profile Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoProfileActionError unless @options.action
		throw new Error \InvalidProfileActionError unless @options.action in settings.allowed_actions

		@api = new API @options, @debug, @test

	run: (args) ->
		Promise.resolve yes .bind @	

		.then -> @api.get 'getCiviVars'

		.then -> @_cvars = it

		.then -> @_map_ids args

		.then -> @[@options.action] it

		.caught -> error: if _.isObject ( err = it?.message or it ) then JSON.stringify err else err

	get: -> # it -> args from run arguments
		@api.get settings.get_api, UCRN: it.ucrn
			.then -> it?.values?[ _.keys(it?.values)?[0] ]

	create: -> # it -> new_args from map_ids
		@api.post settings.create_api, null, param @_createPayload it
			.then -> it?.values?[ _.keys(it?.values)?[0] ]

	update: (args) -> # it -> new_args from map_ids
		@get args .bind @ .then -> 
			@api.post settings.update_api, null, param @_updatePayload args, it
				.then -> it?.values?[ _.keys(it?.values)?[0] ]

	_updatePayload: (args={}, contact={}) ->

		payload = contact_id: contact.contact_id
		_.extend payload, @_clean @_createPayload(args), yes

		_.each payload.phone, (v, i) ->
			obj = _.find contact.phone, phone_type_id: v.phone_type_id
			payload.phone[i] = _.extend obj, v if obj

		_.each payload.email, (v, i) ->
			obj = _.find contact.phone, is_primary: v.is_primary
			payload.email[i] = _.extend obj, v if obj

		_.each contact.address, (v) ->
			payload.custom_Address.push v

		@_clean payload, yes

	_createPayload: (args={}) ->
		@_clean {
			"prefix_id": args.title,
			"suffix_id": args.suffix,
			"gender_id": args.gender,
			"last_name": args.last_name,
			"first_name": args.first_name,
			"birth_date": args.birth_date,
			"contact_type": args.contact_type,
			"preferred_language":  args.preferred_language,

			"phone": [{
				"is_primary": "0",
				"is_billing": "0",
				"phone_type_id": "1",
				"location_type_id": "1",
				"phone": args.phone_number
			}, {		
				"is_primary": "1",		
				"is_billing": "0",
				"phone_type_id": "2",
				"location_type_id": "1",
				"phone": args.mobile_number
			}, {		
				"is_primary": "0",		
				"is_billing": "0",
				"phone_type_id": "3",
				"location_type_id": "1",
				"phone": args.alternative_number
			}],
			"email": [{
				"is_primary": "1",
				"is_billing": "0",
				"is_bulkmail": "0",
				"location_type_id": "1",
				"email": args.email_address
			}, {
				"is_primary": "0",
				"is_billing": "0",
				"is_bulkmail": "0",
				"location_type_id": "1",
				"email": args.alternative_email
			}],
			"custom_Address": [{
				"Flat": args.flat,
				"Town": args.town,
				"UPRN": args.uprn,
				"Ward": args.ward,
				"House": args.house,
				"Street": args.street,
				"County": args.county,
				"Locality": args.locality,
				"Postcode": args.postcode,
				"country_id": args.country,
				"Post_Town": args.postal_town,
				"Search_for_street_Postcode": "#{args.house?.trim?!}::#{args.street?.trim?!}::#{args.postcode?.trim?!}",

				"Location": "4",
				"is_primary": "1",
				"location_type_id": "1",
				"address_type": args.address_type or "1",
				"Start_Date_": Date.today?!.toString? 'yyyy-MM-dd HH:mm:ss'
			}],
			"custom_Additional_Details": {
				"Preferred_Contact_Method": args.preferred_contact_method
				"Consent_To_Share_": if args.consent_to_share then "1" else "0"
			}
		}

	_map_ids: (args={}, rev) -> # Map Civi ID's to Text in args e.g. Mr. -> 3 and with rev the other way 3 -> Mr.
		return args if @options.action is 'get'

		throw new Error "['Profile'] ['Map'] No Civi Vars" unless _.isObject @_cvars

		new_args = _.extend _.cloneDeep(args), do 
			consent_to_share: not not args.consent_to_share
			status : _.find @_cvars.statuses, name: "New" ?.value

		plurals =
			title: \titles
			suffix: \suffix
			gender: \genders
			country: \countries
			# preferred_language: \languages
			preferred_contact_method: \preferred_contact_methods

		_.each plurals, (plural, key) ~>
			return new_args[key] = _.find @_cvars[plural], name: args[key] ?.label if rev
			new_args[key] = _.find @_cvars[plural], name: args[key] ?.value
			return true
		return new_args		

	_clean: (payload, del) ->
		_.each payload, (v, i) ~>
			return @_clean v, del if _.isObject v
			payload[i] = v = v?.trim?! or ''
			payload[i] = 'Organization' if i is 'contact_type' and v?.toLowerCase?! is 'organisation' 
			delete payload[i] if del and payload[i] is ''
			return true
		return payload


