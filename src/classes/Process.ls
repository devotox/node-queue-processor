require! {
	_: \lodash
	Promise: \bluebird
	Validator: \validator
}

API = require "./Api"
require "./../libs/date"
utils = require "./utils"
Tokenizer = require "./Tokenizer"
Integration = require "./Integration"
Notification = require "./Notification"

module.exports = class Process

	settings =
		open: 1
		closed: 0

	defaults = 
		stage: null
		process: null
		category: null
		throwError: yes
		global_tokens: {}
		customer_id: null
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Process Options \n', options

		@options = _.defaults options, defaults
		throw new Error \NoStageError unless @options.stage and not _.isEmpty @options.stage
		throw new Error \NoProcessError unless @options.process and not _.isEmpty @options.process
		throw new Error \NoReferenceError unless @options.reference and not _.isEmpty @options.reference
		throw new Error \NoSubmissionError unless @options.submission and not _.isEmpty @options.submission

		@stage = @options.stage
		@process = @options.process
		@category = @options.category
		@reference = @options.reference
		@submission = @options.submission
		@isPublished = @submission.isPublished

		@summary = JSON.stringify @submission.summary or []
		@created_date = new Date! .toString "yyyy-MM-dd HH:mm:ss"

		@case_id = if @submission.new_case then null else @submission.case_id
		@new_case_id = if @submission.new_case then @submission.case_id else "FS-Case-#{@reference.replace /^\D+/g, ''}"

		@api = new API @options, @debug, @test
		
		@options.global_tokens = @options.global_tokens ?= @submission.global_tokens
		@options.global_tokens = _.extend @options.global_tokens, 
			task_id: @reference
			task_ref: @reference
			case_id: @case_id or @new_case_id
			case_ref: @case_id or @new_case_id

		@tokenizer = new Tokenizer do
			tokens: @submission.tokens
			env_tokens: @submission.env_tokens
			global_tokens: @options.global_tokens
			values: @submission.formValues or @submission.values

	run: ->
		Promise.resolve true .bind @

		.then -> console.info "['Process'] ['#{@created_date}'] #{@case_id or @new_case_id} started..."

		.then -> @previousData!

		.then -> @currentTask!

		.then -> @case_id or= @new_case_id

		.then -> @nextTask!

		.then -> @updateCase!

		.then (data) ->
			verb = if @case_id is @new_case_id then "Created" else "Updated"
			console.info "['Process'] #{@case_id} #{verb}..."
			return data

		.caught -> 
			console.error err = "['Process'] #{it?message or it}"
			throw new Error err if @options.throwError

	setSummaryAndAggregateData: (data) ->
		if data and _.isObject data
			try @previous_summary = JSON.parse data.case_summary or '{}'
			try @previous_aggregate = JSON.parse data.aggregate_data or '{}'
		
		@case_aggregate = @aggregate!
		@case_summary = @summarize @case_aggregate
		@task_summary = @summarize @submission.formValues

	previousData: ->
		Promise.resolve true .bind @

		.then -> 
			console.info "['Process'] ['Previous Data'] Get Previous Data from Case #{@case_id}..." if @case_id
		
		.then -> 
			if @case_id then @getCase @case_id
			else originator: @submission.user?.email or @options?.citizen?.email or '0'

		.then -> 
			data = it?[0] or it or {}
			@originator = data.originator
			@setSummaryAndAggregateData data

		.caught -> throw new Error "['Previous Data'] #{it?.message or it}"

	currentTask: ->
		Promise.resolve yes .bind @

		.then -> if @case_id then @updateTask! else @createFirstTask!

		.caught -> throw new Error "['Current Task'] #{it?.message or it}"		

	createFirstTask: ->

		case_data = 
			new_id: @new_case_id # New Case - Submission Reference used as case id

		task_data =
			task_id: @reference
			case_id: @new_case_id
			data: @case_aggregate
			summary: @task_summary
			originator: @originator
			task_status: settings.closed # Close New Task after creation

		Promise.resolve yes .bind @

		.then -> @integrations @stage, settings.closed

		.then -> 		
			console.info "['Process'] ['Create First Task'] Creating Case..."
			@api.post \/cases, null, @createCaseArgs case_data

		.then -> @checkResponse it

		.then -> 
			console.info "['Process'] ['Create First Task'] Creating Task with ID: #{@stage.props.db_id}..."
			@api.post \/task, null, @createTaskArgs task_data
		
		.then -> @checkResponse it

		.caught -> throw new Error "['Create First Task'] #{it?.message or try JSON.stringify it}"

	nextTask: (stage=@stage) ->
		tasks = []
		return unless stage?.routes
		db_ids = [ stage.props.db_id ]

		setProps = (next_stage) ~>
			next_stage.props.db_id = utils.guid! if next_stage.props.db_id in db_ids
			qurl = next_stage.props.queryString + next_stage.props.db_id

			return next_stage.props.TaskURL = next_stage.props.PublicTaskURL = qurl unless @isPublished
		
			next_stage.props.PublicTaskURL = "/AchieveForms/#{qurl}"
			next_stage.props.TaskURL = "/build/fillform_newAF.html#{qurl}"

		_.each stage.routes, (route, i) ~>
			return true unless next_stage = _.cloneDeep @process.stages[i]
			
			setProps next_stage	
			db_ids.push next_stage.props.db_id
			tasks.push @createTask next_stage, previous_task_db_id: stage.props.db_id
			console.info "['Process'] ['Next Task'] Creating Task with ID: #{next_stage.props.db_id}..."
			return true	

		Promise.all tasks .bind @

		.caught -> throw new Error "['Next Task'] #{it?.message or try JSON.stringify it}"

	createTask: (stage=@stage, data) ->

		Promise.resolve yes .bind @

		.then -> @notify stage, settings.open if @case_id

		.then -> @integrations stage, settings.open if @case_id

		.then -> 
			console.info "['Process'] ['Create Task'] Creating Task..."
			@api.post \/task, null, @createTaskArgs data, stage

		.then -> @checkResponse it

		.caught -> throw new Error "['Create Task'] #{it?.message or try JSON.stringify it}"

	updateTask: (stage=@stage, data) ->

		Promise.resolve yes .bind @

		.then -> @notify stage, settings.closed if @case_id

		.then -> @integrations stage, settings.closed if @case_id

		.then -> 
			console.info "['Process'] ['Update Task'] Updating Task..."
			@api.post \/task, null, @updateTaskArgs data, stage .bind @

		.then -> @checkResponse it

		.caught -> throw new Error "['Update Task'] #{it?.message or try JSON.stringify it}"

	updateCase: (stage=@stage, data={}) ->

		Promise.resolve yes .bind @

		.then -> @api.get \/task, case_id: @case_id, task_status: settings.open

		.then -> data.case_status = if _.size it then settings.open else settings.closed

		.then -> 
			verb = if data.case_status is settings.closed then "Closing" else "Updating"
			console.info "['Process'] ['Update Case'] #{verb} Case #{@case_id}..." 
			@api.post \/cases, null, @updateCaseArgs data, stage

		.then -> @checkResponse it

		.then -> @getCase @case_id, data.case_status

		.caught -> throw new Error "['Update Case'] #{it?.message or try JSON.stringify it}"

	getCase: (case_id=@case_id, case_status="1") ->

		Promise.resolve yes .bind @
		
		.then -> 
			console.info "['Process'] ['Get Case'] Get Case #{case_id} with status #{case_status}..."
			@api.get \/cases, action: \get, case_id: case_id, case_status: case_status, aggregate: true, summary: true

		.caught -> throw new Error "['Get Case'] #{it?.message or try JSON.stringify it}"

	checkResponse: (data={}) ->
		throw new Error data.message if data.success is false
		throw new Error data.error if data.error
		return data

	summarize: (values) ->
		return unless values; summary = []
		values = JSON.parse values if _.isString values

		_.each values, (section) ->
			return true unless _.size section
			_.each section, (field) ->
				return true unless field?.isSummary
				summary.push do
					name: field.name
					label: field.label
					value: field.value
					summary: field.summary
					value_label: field.value_label
		return JSON.stringify summary

	aggregate: ->
		return JSON.stringify @submission.formValues unless _.size @previous_aggregate

		@previous_aggregate = JSON.parse @previous_aggregate if _.isString @previous_aggregate
		agg_values = _.merge {}, @previous_aggregate, _.cloneDeep @submission.formValues
		new_values = _.merge {}, _.cloneDeep @submission.formValues

		all_fields = []

		for own x, agg_section of agg_values
			for own x, agg_field of agg_section
				
				agg_name = agg_field.name				
				( delete agg_section[x]; continue ) if agg_name in all_fields

				for own x, section of new_values
					for own x, field of section
						( agg_section[x] = field; break ) if field.name is agg_name
				
				all_fields.push agg_name							

		return JSON.stringify agg_values

	notify: (stage=@stage, open, notification) ->
		return unless notification or= stage?.props?.notification
		return unless notification.recipients_to or notification.notify_assigned_users
		return if open is settings.closed

		notification.recipients_to or= ''
		notification.subject or= "#{stage.name} Created"
		notification.body or= "New Task Created - #{@reference}"

		if notification.notify_assigned_users in [ true, 'true' ]
			n = notification.recipients_to.split(',')
			n.push stage.props.user
			notification.recipients_to = n.join ','

		recipients = _.map notification.recipients_to.split(','), (email) ~> return [ '', @checkUserToken( email?.trim?! ), 'To' ]
		( recipients = _.filter recipients, (rec) -> return rec?[1] and rec?[2] and Validator.isEmail rec?[1] ) if recipients?.length
		return unless recipients?.length

		opts = {}
		opts.API =
			docapi_url: @options.docapi_url
			customer_id: @options.customer_id
			apibroker_url: @options.apibroker_url
		opts.template =
			html: true, type: \email, recipients: recipients
			from: [ \notifications@firmstep.com, \Firmstep ]
			subject: notification.subject, message: "<div>#{notification.body}</div>"
		opts.LIM = id: notification?.LIM_id if notification.LIM_id

		console.info "['Process'] Sending Notification to #{try JSON.stringify recipients}"
		
		new Notification opts, @debug, @test .send! .bind @

		.caught -> throw new Error "['Notification'] #{it?.message or it}"

		.caught -> console.error it?.message or it

	integrations: (stage=@stage, open) ->
		if open is settings.open then integrations = stage.props.creationActions
		else integrations = stage.props.submissionActions

		return unless integrations?.length
		_submission = _.cloneDeep @submission

		_global_tokens = _.merge {}, @options.global_tokens, do
			PublicTaskURL: stage.props.PublicTaskURL # "<a href='#{if open then stage.props.PublicTaskURL else ''}'>Task #{stage.name}</a>"
			TaskURL: stage.props.TaskURL # "<a href='#{if open then stage.props.TaskURL else utils.fromBase64 _submission.site}'>Task #{stage.name}</a>"

		opts =			
			throwError: yes
			return_values: yes
			submission: _submission
			integration: integrations
			global_tokens: _global_tokens
			customer_id: @options.customer_id

		new Integration opts, @debug, @test .run! .bind @

		.spread (data, transformed, values, runtimes) -> 
			@submission.formValues = values
			@setSummaryAndAggregateData!

		.caught -> throw new Error "['Integration'] #{it?.message or it}"

		.caught -> console.error it?.message or it

	updateCaseArgs: (data={}, stage=@stage, submission=@submission) ->
		args = 
			action: \set
			case_id: @case_id
			case_summary: @case_summary
			aggregate_data: @case_aggregate
		_.extend args, data

	updateTaskArgs: (data={}, stage=@stage, submission=@submission) ->
		args = 
			action: \set
			task_id: @reference
			id: submission.db_id
			summary: @task_summary
			task_status: settings.closed
			data: JSON.stringify submission.formValues
			
			csa_id: @options?.csa?.id
			csa_name: @options?.csa?.name
		_.extend args, data

	createCaseArgs: (data={}, stage=@stage, submission=@submission) ->
		args = 
			action: \set
			new_id: @case_id
			category: @category
			case_status: settings.open

			process_id: @process.props.id
			process_name: @process.processName

			case_summary: @case_summary
			aggregate_data: @case_aggregate
			published: if submission.isPublished then 1 else 0

			originator: @originator
			view_group_id: @getViewGroup @process
			citizen_id: @options?.citizen?.id or submission.user?.user_id
			citizen_name: @options?.citizen?.name or submission.user?.name
			citizen_email: @options?.citizen?.email or submission.user?.email
		_.extend args, data

	createTaskArgs: (data={}, stage=@stage, submission=@submission) ->
		args = 
			action: \set
			task_id: null
			case_id: @case_id
			previous_task_db_id: null
			new_id: stage.props.db_id
			task_status: settings.open

			data: null
			summary: null	

			stage_id: stage.id 
			stage_name: stage.name
			stage_form: stage.props.form

			assigned_user_id: @getUser stage
			assigned_group_id: @getGroup stage

			escalation_user_id: @getUser stage, true
			escalation_group_id: @getGroup stage, true
			escalation_status: if stage.props?.escalation?.date then \1 else \0
			date_escalation: Date.parse stage.props?.escalation?.date, "yyyy-mm-dd HH:mm:ss"
			date_escalation_warn: Date.parse stage.props?.escalation?.date_warn, "yyyy-mm-dd HH:mm:ss"
			
			csa_id: if data.task_status is settings.closed then @options?.csa?.id
			csa_name: if data.task_status is settings.closed then @options?.csa?.name
		_.extend args, data

	getUser: (stage, escalation) ->
		unless escalation
			user = @tokenizer.getValue stage.props.user_token
			user or= stage.props.user or ''
			user = @checkUserToken user
		else 
			user = @tokenizer.getValue stage.props.escalation?.user_token
			user or= stage.props.escalation?.user or ''
			user = @checkUserToken user
		return user

	getGroup: (stage, escalation) ->
		unless escalation
			group = @tokenizer.getValue stage.props.group_token
			group or= stage.props.group or ''
		else 
			group = @tokenizer.getValue stage.props.escalation?.group_token
			group or= stage.props.escalation?.group or ''
		return group


	getViewGroup: (process) ->
		group = @tokenizer.getValue process.props.viewGroupToken
		group or= process.props.viewGroup or ''
		return group

	checkUserToken: (user) ->
		user = @originator if user is '__originator__'
		return user

