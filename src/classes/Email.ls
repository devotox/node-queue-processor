require! {
	_: \lodash
	Promise: \bluebird
}

utils = require "./utils"
Notification = require "./Notification"

module.exports = class Email

	defaults = 
		"link": null
		"site": null
		"save-id": null
		"user-name": ''
		"form-name": null
		"user-email": null
		"customer-id": null

		"send-mode": \To
		"send-mail": true
		"from-name": \Firmstep,
		"from": \notifications@firmstep.com
		"subject": (options) -> "Continue Saved Form: #{options['form-name']}"
		"message": (options) -> "<p>Hello #{options['user-name'] or 'User'}</p><p>You may access your saved form for #{options["form-name"]} at</p>"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Email Options \n', options

		options = @decode options
		@options = _.defaults options, defaults

		throw new Error \NoSiteError unless @options.site
		throw new Error \NoLinkError unless @options.link
		throw new Error \NoSaveIdError unless @options['save-id']
		throw new Error \NoFormError unless @options['form-name']
		throw new Error \NoUserEmailError unless @options['user-email']
		@notification = new Notification @createTemplate!, @debug, @test

	send: ->
		return Promise.resolve [ @options['user-email'], false ] unless @options['send-mail']

		@notification.send! .bind @

		.then -> [ @options['user-email'], @save_link ]

		.caught -> throw new Error ( it.message or it )

	decode: (options) ->
		options['send-mail'] = try JSON.parse options['send-mail'] catch then true
		_.each ['from', 'from-name', 'subject', 'message', 'site', 'link'], (v) ~>
			if options[v] then options[v] = @fromBase64 options[v] 
			else delete options[v]
		return options

	fromBase64: (val) -> 
		return val unless val
		try decodeURIComponent escape utils.fromBase64 val
		catch e then return val

	createTemplate: ->	
		note = 
			API: customer_id: @options['customer-id']
			template: 
				html: true, type: \email,
				subject: @createSubject!
				message: @createMessage!
				from: [ @options['from'], @options['from-name']  ]
				recipients: [ [ @options["user-name"], @options["user-email"], @options['send-mode'] ] ]

	createSubject: ->
		@options["subject"]?(@options) or @options['subject']

	createMessage: ->
		patt = /(\{SaveURL\}|\{save_url\})/gmi

		message = @options.message?(@options) or @options.message
		message = "<div>#{message?.replace?(/(&nbsp;|\n)+/g, '') or message}</div>"

		link = "<p><a href='#{@save_link = @options.link or @createLink!}'>Continue #{@options['form-name']}</a></p>"
		
		unless message.match patt then message = message + link
		else message = message.replace patt, link
		return message

	createLink: ->
		hashparts = @options["site"]?.split "#"

		_url = hashparts.shift!
		urlparts = _url.split "?"		
		urlparts = [ urlparts.shift! , urlparts.join '?' ] if urlparts.length > 2

		[ site, query ] = urlparts

		query = if query then "&#{query}" else ''
		hash = if hashparts.length then "##{hashparts.join('#')}" else ''
		link = [ site, "?save_id=", @options["save-id"], "&noLoginPrompt=1", query, hash ].join ''
		return link
