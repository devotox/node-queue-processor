require! {
	_: \lodash
	crypto: \crypto
	Promise: \bluebird
	validator: \validator
}


API = require "./Api"
require "./../libs/date"
utils = require "./utils"

module.exports = class Validator

	defaults = 		
		form: null
		submission: null
		customer_id: null
		docapi_url: \http://localhost/docapi/
		apibroker_url: \http://localhost/apibroker/

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Validator Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoCustomerIdError unless @options.customer_id
		throw new Error \NoFormError unless @options.form and not _.isEmpty @options.form
		throw new Error \NoSubmissionError unless @options.submission and not _.isEmpty @options.submission

		@api = new API @options, @debug, @test

	validate: -> 
		Promise.resolve true .bind @
		
		.then -> @payment!

		.then -> @validateForm!

		.then -> @validateProcess!

		.caught -> throw new Error (it.message or it)

	validateForm: ->
		# break up validation methods and initiate here 
		submission = @options.submission

		return true

	validateProcess: ->
		# break up validation methods and initiate here 
		submission = @options.submission

		return true

	payment: ->
		submission = @options.submission

		return true unless submission.trans_id and submission.int_id and submission.hash and submission.query
		
		@api.get \/user-packages, action: \get, id: submission.int_id .bind @

		.then -> @_getConnector it

		.then -> @_checkHash it, submission

	_getConnector: (integration) ->
		try template = JSON.parse integration.Output_template
		catch err then template = integration.Output_template or integration
		throw new Error \NoPaymentConnector unless connector = template?.PaymentConnector
		
		connector = connector?.toLowerCase!?.replace \payment, ''
		connector = \capitav8 if connector is \capita
		return connector

	_checkHash: (connector, submission, opts={}) ->
		hash = opts.hash or submission.hash
		query = opts.query or submission.query

		@api.get \/payment-types, action: \get_by_name, name: connector .bind @

		.then (data) ->
			throw new Error \InvalidPaymentConnector unless _.size data
			
			hash := hash.toUpperCase!
						
			unhashed = data.pre_salt + query + data.post_salt
			_unhashed = data.pre_salt + query.toLowerCase! + data.post_salt

			chash = crypto.createHash \sha512 .update unhashed .digest \hex .toUpperCase!
			_chash = crypto.createHash \sha512 .update _unhashed .digest \hex .toUpperCase!

			if @debug and not @test
				console.info '<<======================= PAYMENT ==========================>>'
				console.info 'Query:', query, '\n'
				console.info 'Salts:', data.pre_salt, data.post_salt,'\n'
				console.info 'Unhashed Query With Salts:', unhashed, '\n'
				console.info 'Unhashed Query With Salts (lowercase):', _unhashed, '\n'
				console.info 'Hash From Query:', chash, '\n'
				console.info 'Hash From Query (lowercase):', _chash, '\n'
				console.info 'Hash From Connector:', hash, '\n'						
				console.info '<<======================= PAYMENT ==========================>>'

			(hash is chash) or (hash is _chash) or throw new Error \InvalidPaymentHash

