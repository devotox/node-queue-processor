
require! {
	_: \lodash
	events: \events
	sqs: \sqs-queue-parallel
}

root = "../"
require "#{root}libs/date"
# sqs = require "#{root}libs/sqs-queue-parallel"

module.exports = class Queue extends events.EventEmitter

	defaults = 
		debug: false
		concurrency: 1
		waitTimeSeconds: 20
		visibilityTimeout: 300
		maxNumberOfMessages: 1

		name: null
		region: null
		accessKeyId: null
		secretAccessKey: null

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Queue Options \n', options
			_.extend options, debug: @debug
			
		@options = _.defaults options, defaults

		throw new Error \NoRegionError unless @options.region
		throw new Error \NoQueueNameError unless @options.name
		throw new Error \NoAccessKeyError unless @options.accessKeyId
		throw new Error \NoSecretKeyError unless @options.secretAccessKey

		@queue = new sqs @options
		@client = @queue.client

		@setDebugEvents! if @debug
		@setEvents!

	restart: (new_options={}) ->
		@options = _.defaults new_options, @options

		@queue = new sqs @options
		@client = @queue.client

		@setDebugEvents! if @debug
		@setEvents!

	add: (message, cb) ->
		@queue.sendMessage message, cb

	delete: (handle, cb) ->
		@queue.deleteMessage handle, cb

	setEvents: ->
		@queue.on 'error', (e) ~>
			console.info @_inspect e, true
			_.delay (~> @restart! ), 5000
			# @emit 'error', e

		@queue.on 'message', (e) ~>
			console.info @_inspect e if @debug and not @test
			@emit e.data.action, e if e.data?.action
			e.next?!

	setDebugEvents: ->
		@queue.on 'connect', (e) ~>
			console.info "['Queue'] Connected - URL:", e 

		@queue.on 'connection', (e) ~>
			console.info "['Queue'] Found Closest URL for #{@options.name}:", e[0]

	_inspect: (e, isError) ->
		date = "[#{new Date().toString('HH:mm:ss dd/MM/yyyy')}]"
		return [ "['Queue']", date, "Queue Error:", e ].join ' ' if isError
		return [ "['Queue']", date, "New Message:", e?.data?.action, e?.data?.reference ].join ' '



