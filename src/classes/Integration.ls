require! {
	_: \lodash
	xslt: \node_xslt
	Promise: \bluebird
	libxmljs: \libxmljs
	json2xml: \json2xml
}

docroot = "../../../../"

API = require "./Api"
require "./../libs/date"
utils = require "./utils"
Profile = require "./Profile"
Tokenizer = require "./Tokenizer"
Permission = require "./Permission"
Notification = require "./Notification"
LimConnect = require "#{docroot}FS-LIM-Connect/build/src/app"

module.exports = class Integration

	settings = 
		timeout: 30
		max_retries: 5
		file_modes: 
			None: false
			New: \Unique
			Append: \Append
			Overwrite: \Overwrite
		base64_types: <[ printable ]>
		files_endpoint: \sandbox-files://
		http_types: <[ webservice http ]>
		print_types: <[ printable email ]>
		no_lim: <[ printable case payment ]>
		map_types: <[ mapSingle mapMultiple ]>
		transform_types: <[ http webservice database ]>
		results_page_types: <[ http webservice database ]>
		printable_style: "body { font-family: Arial, 'Open Sans', sans-serif; font-size: 14px; color: #444 }"

	defaults = 
		noLog: no
		isTest: no
		isLookup: no
		throwError: no
		template: false
		submission: null
		integration: null
		customer_id: null
		return_values: no
		debug_runtimes: no
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Integration Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoSubmissionError unless @options.submission and not _.isEmpty @options.submission
		throw new Error \NoIntegrationError unless @options.template or ( @options.integration and not _.isEmpty @options.integration )

		@stopOnFailure = @options.submission?.stopOnFailure ?= no
		@integrations = _.flatten [ @options.integration or '' ]
		settings.timeout = if @options.isLookup then 30 else 60
		@debug_runtimes = @options.debug_runtimes
		@submission = @options.submission
		@runtimes = {}
		@current = {}

		@api = new API @options, @debug, @test

		@tokenizer = new Tokenizer do
			tokens: @submission.tokens
			values: @submission.formValues
			env_tokens: @submission.env_tokens
			global_tokens: @options.global_tokens
		
		@start = null

	elapsed_time: (note) ->
		return unless note
		precision = 3 # 3 decimal places
		elapsed = process.hrtime(@start)[1] / 1000000 # divide by a million to get nano to milli
		console.info "['Integration'] ['Timing']", elapsed.toFixed(precision) + " ms - " + note unless @test # print message + time
		@runtimes[note] = elapsed.toFixed(precision) + "ms"
		@start = process.hrtime! # reset the timer

	logSuccess: (data) ->
		return Promise.resolve true if @options.isTest or @options.noLog

		console.info "['Integration'] ['Log Success'] #{JSON.stringify data}" if @debug and not @test
		log = @_createLog data 

		@api.post "/LogIntegration", null, log		
		.caught -> console.info "['Integration'] ['Log Success Error']", it if @debug and not @test

	logError: (err, isNotification) ->
		return Promise.resolve @_notify err if @options.noLog		
		return Promise.resolve true if @options.isTest or @options.noLog

		err = err.message or err
		@_notify err unless isNotification

		console.info "['Integration'] ['Log Error'] #{err}" if @debug and not @test
		log = if isNotification then err else @_createLog err, true

		@api.post "/LogIntegration", null, log
		.caught -> console.info "['Integration'] ['Log Error Error']", it if @debug and not @test

	run: (template, lim) ->

		template ?= @options.template

		@current.integration = @integrations?.shift! or template?.integration

		throw new Error \NoCurrentIntegration unless @current.integration

		throw new Error \NoCurrentIntegrationID unless @current.integration?.id

		@tokenizer.options.global_tokens.integration_id = @options.global_tokens.integration_id = @current.integration?.id

		Promise.resolve true .bind @

		.then -> @_getIntegrationData template, lim

		.then -> 
			return if @fields_replaced
			if @options.isLookup then @_replaceLookupUpload! else @_replaceUpload!

		.then -> @fields_replaced = true

		.then -> @_replacePrintables!

		.then -> @_runIntegration!

		.then (data) ->
			orig_data = data
			throw new Error err if err = @_checkResponse data
			@logSuccess data = @_normalizeResponse data .then -> [ data, orig_data ]

		.spread (data, orig_data) ->

			@_updateSubmissionValues orig_data unless @options.isLookup
			transformed_data = try @tokenizer._transform orig_data, @current.out_template
			console.info "['Integration'] ['#{@current.type?.toUpperCase!}'] Finished:", @current.name unless @test

			if @integrations?.length
				try @_setPrevData data
				return @run! 

			func = if @options.isLookup then Promise.resolve true else @_resetUpload!
			func .bind @ .then -> 
				return data unless @options.return_values
				return [ data, transformed_data, @submission.formValues, @runtimes ]

		.caught (err) ->
			@logError err .bind @ .then ->
				throw new Error ( err?.message or err ) if @options.throwError
				return @run! if _.size @integrations and not @stopOnFailure
				return err

	_runIntegration: (retries=0) ->

		Promise.resolve true .bind @

		.then -> @_setCurrent!

		.then ->	
			rep_value = @_getRepeatValue!	

			return @_once! unless _.isArray rep_value
			return @_repeat rep_value

		.caught Promise.TimeoutError,  (err) ->
			throw new Error "[#{@current.type?.toUpperCase!}] ['Integration Timeout Error'] (#{settings.timeout} secs)"

		.caught ->
			retries := parseInt(retries) or 0
			throw new Error (it?.message or it) if @options.isTest or retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @_runIntegration retries
	
	_once: ->
		@_replaceTokens!

		if @debug and not @test
			console.info "['Integration'] Integration Type:", @current.type
			console.info "['Integration'] Current Tokens\n", @current.tokens
			console.info "['Integration'] Current Out Template\n", @current.out_template 
			console.info "['Integration'] Current Calculated Template\n", @current.calc_template

			if _.isArray @current.calc_template?.calc_query
				console.info "['Integration'] Current Calculated Query\n", @current.calc_template.calc_query[0]
				console.info "['Integration'] Current Calculated Parameters\n", @current.calc_template.calc_query[1]

		console.info "['Integration'] ['#{@current.type?.toUpperCase!}'] Running:", @current.name unless @test
		
		@_types[@current.type].call @, @current.calc_template 

		.bind @ .timeout settings.timeout * 1000

		.caught (err) -> 
			@current.limConnect?._response ?= err
			throw new Error "['#{@current.type?.toUpperCase!}']: #{err.message or err}"

	_repeat: (rep_value, index=0) ->
		console.info "['Integration'] Running Repeatable Integration", _.size(rep_value), 'Times' if @debug and not @test

		@tokenizer.af_tokens?.ListID = index + 1
		ra_name = @current.integration.repeat_against
		@tokenizer.setFieldValue ra_name, [ rep_value[ index ] ]

		_finish = (data, isError) ~>
			delete @tokenizer.af_tokens?.ListID
			@tokenizer.setFieldValue ra_name, rep_value
			if isError then throw new Error data else return data

		@_once! .bind @

		.then (data) ->
			return _finish data unless rep_value[++index]

			throw new Error err if err = @_checkResponse data
			@logSuccess data = @_normalizeResponse data
			try @_setPrevData data
			@_repeat rep_value, index

		.caught (err) -> 
			err = err.message or err
			err = "['#{@current.type?.toUpperCase!}'] ['Repeat Against Error'] ['Field: #{@current.integration.repeat_against}'] #{err}"

			return _finish err, yes unless rep_value[++index] and not @stopOnFailure

			@logError err
			@_repeat rep_value, index 

	_getIntegrationData: (template, lim, retries=0) ->

		Promise.cast yes .bind @

		.then ->
			@start = process.hrtime! 
			@_getTemplate template

		.then -> 
			@elapsed_time("get-template-#{@current.integration.id}") if @debug_runtimes
			@current.template = it

		.then -> 
			@start = process.hrtime! 
			@_getLim lim

		.then -> 
			@elapsed_time("get-lim-#{@current.integration.id}") if @debug_runtimes
			@current.lim = it

		.caught ->
			retries := parseInt(retries) or 0
			throw new Error (it?.message or it) if @options.isTest or retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @_getIntegrationData template, lim, retries

	_getTemplate: (template) ->
		console.info "['Integration'] Getting Template @ #{@current.integration?.id}" if @debug and not @test
		
		return Promise.resolve template if template

		@api.get "/user-packages", action: 'get', id: @current.integration?.id .bind @

		.then -> it?[0] or it

		.caught -> throw new Error "['No Integration Error'] ID: #{@current.integration?.id} || Error: #{it?.message or it?.toString?!} || Current Integration: #{JSON.stringify @current}"

	_getLim: (lim) ->
		console.info "['Integration'] Getting LIM @ #{@current.template?.LIM_id}" if @debug and not @test
		
		return Promise.resolve lim if lim

		@api.get "/lims", action: 'get', id: @current.template?.LIM_id .bind @

		.then -> it?[0] or it

		.caught -> throw new Error "['No LIM Error'] ID: #{@current.template?.LIM_id} || Error: #{it?.message or it?.toString?!} || Current Integration: #{JSON.stringify @current}"

	_createLog: (data, isError, id) ->
		log = 		
			form_id: @submission.formId
			form_uri: @submission.formUri
			form_name: @submission.formName
			published: @submission.isPublished

			error: if isError then 1 else 0
			data: if isError then null else data
			error_message: if isError then data else null

			customer_id: @options.customer_id
			reference: @options.global_tokens.reference

			lim_id: @current.template?.LIM_id
			lim_name: @current.template?.LIM_name
			integration_id: @current.integration?.id
			requested_timestamp: @current.requested_timestamp

			template: @current.out_template
			duration: @current.limConnect?._duration
			request_xml: if _.isObject @current.limConnect?._request then JSON.stringify @current.limConnect?._request else @current.limConnect?._request
			response_xml: if _.isObject @current.limConnect?._response then JSON.stringify @current.limConnect?._response else @current.limConnect?._response

		console.info "['Integration'] Integration Log:\n", log if @debug and not @test
		return log
		
	_setCurrent: -> # Parse JSON fields here
		@current.name = @current.template.Name
		@current.type = @current.template.Type?.toLowerCase!
		@current.requested_timestamp = new Date! .toString 'dd/MM/yyyy hh:mm tt'

		try @current.out_template = JSON.parse @current.template['Output_template']
		catch err then @current.out_template = @current.template['Output_template'] or {}

		if @current.type is 'permission'
			try @current.out_template.user_groups = JSON.parse @current.out_template.user_groups
			@current.out_template.user_groups = _.flatten [ @current.out_template.user_groups ]

		if @current.type in settings.http_types
			try @current.out_template.fields = JSON.parse @current.out_template.fields
			try @current.out_template.headers = JSON.parse @current.out_template.headers			
			try @current.out_template.payload = JSON.parse(
				@current.out_template.payload
			) if @current.out_template.version is '2' or @current.out_template.payloadType is 'JSON'

		try @current.out_template.results_page = JSON.parse(
			@current.out_template.results_page_columns_db or 
			@current.out_template.results_page_columns or '{}'
		) if @current.type in settings.results_page_types
		
		try 			
			return @current.limConnect = _duration: 0 if @current.type in settings.no_lim
			@current.limConnect = new LimConnect @current.lim, @debug, @test
		catch err then throw new Error "['Setting Current LIM Error'] #{err.message or err}"

	_getRepeatValue: ->
		rep_value = null
		if @current.integration.repeat_against
			rep_value = @tokenizer.getFieldValue @current.integration.repeat_against
			try rep_value = JSON.parse rep_value
			rep_value = _.flatten [ rep_value ]

			console.info "['Integration'] Repeat Against:\n", rep_value if @debug and not @test
		return rep_value

	_updateSubmissionValues: (data) ->
		@submission.formValues = @tokenizer.updateSubmissionValues data, @current.type

	_setPrevData: (data) ->	
		@tokenizer.options.global_tokens.prev_data = data
		prev_data_64 = if data?.content then data?.content else data
		prev_data_64 = if _.isObject prev_data_64 then JSON.stringify prev_data_64 else prev_data_64
		@tokenizer.options.global_tokens.prev_data_64 = if @current?.type in settings.base64_types then prev_data_64 else utils.toBase64 prev_data_64

	_replaceTokens: ->
		@current.tokens = @tokenizer.getTokens @current.out_template
		@current.calc_template = @tokenizer.replaceTokens @current.out_template, @current.type in settings.print_types, ( ( @current.type in ['email'].concat settings.http_types ) and ( @current.out_template?.payloadType or 'XML' ) )
		@current.calc_template.calc_query = @tokenizer.parameterizeTokens @current.out_template.query if @current.out_template?.query 

		console.info "['Integration'] Tokens Replaced..." if @debug and not @test
		return @current.calc_template

	_replacePrintables: (values=@submission.formValues, temp={}) ->
		try temp = JSON.parse @current.template.Output_template
		return Promise.resolve true unless temp.pdf_attachments
		try temp.pdf_attachments = JSON.parse temp.pdf_attachments
		catch e then temp.pdf_attachments = temp.pdf_attachments.split \,
		return unless _.size att = _.flatten [ temp.pdf_attachments ] 
		
		ret = []; tasks = []
		_.each att, (v) ~> return true unless v; tasks.push @_getPrintable(v).then ~> ret.push it if it
		Promise.all(tasks).then ~> temp.pdf_attachments = ret; @current.template.Output_template = JSON.stringify temp

	_getPrintable: (id) ->

		@api.get "/user-packages", { action: 'get', id: id } .bind @

		.then -> 
			temp = ( it?[0] or it )?.Output_template
			try temp = JSON.parse temp
			return temp

		.then (temp) ->

			style = settings.printable_style

			@current.type = @current.template.Type?.toLowerCase!

			temp.content = @tokenizer.replaceTokens temp.content, ( @current.type in settings.print_types ), ( ( @current.type in ['email'].concat settings.http_types ) and 'XML' )

			_message = [
				"<html><head>"
				# "<link rel='stylesheet' type='text/css' href='http://localhost/fillform/styles/AchieveForms.min.css'/>"
				"<style type='text/css'> #{style} </style>"
				"</head><body> #{temp.content} </body></html>"
			].join '\n'

			pdf_args = 
				html: utils.toBase64 _message
				name: "#{(temp.name or temp.filename or 'Printable')}.pdf"

			@api.post "/pdf-generator", null, pdf_args, false
			
		.then -> 
			throw new Error it.error if it?.error
			it?.isEncoded = true
			return it
		.caught ->
			console.info "['Integration'] Get Printable Error", it
			null

	_replaceLookupUpload: (values=@submission.formValues) ->
		tasks = []
		_.each values, (section) ~> _.each section, (field) ~>
			if field.type is 'subform' and field.value
				unless _.isArray field.value
					tasks.push @_replaceLookupUpload field.value	
				else for own x, sub of field.value
					try field.value[x] = JSON.parse sub
					tasks.push @_replaceLookupUpload field.value[x]						
				
			return true unless field.type is 'upload' and field.value?.length
			field.value = _.flatten [ field.value ]
			for own x, file of field.value
				try file = JSON.parse file
				field.value[x] = 
					isEncoded: true
					type: file.type
					name: file.name
					content: file.content?.split?("base64,")?[1]

		console.info "['Integration'] Lookup Uploads Replaced..." if @debug and not @test

	_replaceUpload: (values=@submission.formValues) ->
		tasks = []
		_.each values, (section) ~> _.each section, (field) ~>
			if field.type is 'subform' and field.value
				unless _.isArray field.value
					tasks.push @_replaceUpload field.value	
				else for own x, sub of field.value
					try field.value[x] = JSON.parse sub
					tasks.push @_replaceUpload field.value[x]
			return true unless field.type is 'upload' and field.value?.length
			tasks.push @_getFiles(field).then (files) -> field.value = files if files

		Promise.all tasks .bind @ 
		.then ->
			@tokenizer.setValues @submission.formValues
			console.info "['Integration'] Uploads Replaced..."  if @debug and not @test
		.caught -> console.error "['Integration'] Replace Upload Error: #{it?.message or it}"
		
	_getFiles: (field) -> 
		files = []; tasks = []		
		value = field.value = _.flatten [ field.value ]	
		return Promise.resolve value unless value?.length

		_getFile = (uri, index) ~>
			@api.docapi uri .then (data) -> 
				file = data?.content
				return unless file

				files[index] = 
					uri: uri
					isEncoded: true
					type: file.type
					name: file.name or file.filename
					content: (file.content or file.file).split("base64,")[1]

		_.each value, (uri, index) ->
			return true if not uri or uri?.indexOf?(settings.files_endpoint) is -1
			tasks.push _getFile uri, index

		Promise.all tasks .caught -> value

	_resetUpload: (values=@submission.formValues) ->
		tasks = []
		_.each values, (section) ~> _.each section, (field) ~>
			if field.type is 'subform' and field.value
				unless _.isArray field.value
					tasks.push @_resetUpload field.value	
				else for own x, sub of field.value
					try field.value[x] = JSON.parse sub
					tasks.push @_resetUpload field.value[x]
			return true unless field.type is 'upload' and field.value?.length
			tasks.push @_resetFiles(field).then (files) -> field.value = files if files

		Promise.all tasks .bind @ 
		.then ->
			@tokenizer.setValues @submission.formValues
			console.info "['Integration'] Uploads Reset..."  if @debug and not @test
		.caught -> console.error "['Integration'] Reset Upload Error: #{it?.message or it}"

	_resetFiles: (field) ->
		files = []; tasks = []		
		value = field.value = _.flatten [ field.value ]	
		return Promise.resolve value unless value?.length

		_.each value, (file, index) ->
			return true if not file?.uri or file.uri?.indexOf?(settings.files_endpoint) is -1
			tasks.push Promise.resolve files[index] = file.uri

		Promise.all tasks .caught -> value

	_checkResponse: (data) ->
		return data?.error or data?.Responses?.RequestResponse?.Error?.Description if _.isObject data	
		
		try xmlDoc = libxmljs.parseXmlString data?.trim?!	
		return unless xmlDoc

		code = parseInt( xmlDoc.get('//Code')?.text! or xmlDoc.get('//ResponseCode')?.text! )
		err = xmlDoc.get('//Description')?.text! or xmlDoc.get('//ResponseStatus')?.text!
		err = data if err?.toLowerCase?!.trim?! is 'error: no lim response'
		
		return "['#{@current?.type?.toUpperCase!}'] #{err}" if code is 4 or code >= 400

	_normalizeResponse: (data) ->
		try data = @_parseData data unless _.isPlainObject data
		catch err then @logError "['#{@current.type?.toUpperCase!}'] ['Integration Success Normalize Error'] #{err}"
		data = data?[0] if _.isArray data and data.length is 1
		return data

	_parseData: (response) ->	
		ret = []; val = null
		nodes = ['//Body', '//DatabaseResponse','//Filename', '//ResponseStatus']

		response = _.flatten [ response ]
		if response then for own x, xml of response
			continue unless xml and _.isString xml
			xmlDoc = libxmljs.parseXmlString xml.trim?!	
			for x in nodes			
				val = xmlDoc.get(x)?.text!
				val = val?.replace /\\/g, \/ if @current.type is 'file' and val?.indexOf? "\\lim\\"
				break if val
			( ret.push val; break ) if val
		return ret

	_notify: (err) ->	

		try notification = JSON.parse @current.template?.error_notification
		catch err then notification = @current.template?.error_notification
		return unless notification?.recipients_to
		
		err = err.message or err
		recipients = _.map notification.recipients_to.split(','), (email) -> return [ '', email?.trim?!, 'To' ]
		if recipients?.length then recipients = _.filter recipients, (rec) -> return rec?[1] and rec?[2]
		return unless recipients?.length

		opts = 
			API: 
				docapi_url: @options.docapi_url
				customer_id: @options.customer_id
				apibroker_url: @options.apibroker_url

			template: 
				html: true, type: \email, recipients: recipients
				from: [ \notifications@firmstep.com, \Firmstep ]
				subject: notification.subject or "#{@current.template.Name} Integration Error"
			
				message: [
					"<pre>Error: #{err}</pre>"
					
					"<p>Integration: #{@current.template.Name}</p>"
					"<p>Integration ID: #{@current.template.ID}</p>"
					
					"<p>Form: #{@submission.formName}</p>"
					"<p>Form ID: #{@submission.formId}</p>"
					"<p>Site: #{@submission.tokens?.site_url}</p>"

					"<p>Process: #{@submission.processDef?.processName or 'None'}</p>"
					"<p>Process ID: #{@submission.processDef?.props.id or 'None'}</p>"

					"<p>Stage: #{@submission.stage_name or 'None'}</p>"
					"<p>Stage ID: #{@submission.stage_id or 'None'}</p>"

					"<p>User Name: #{@submission.user?.name}</p>"
					"<p>User ID: #{@submission.user?.user_id}</p>"
					"<p>User Email: #{@submission.user?.email}</p>"

					"<p>Date #{if @options.isLookup then \Lookup-Submission else \Submission} Created: #{@submission.created}</p>"
					"<p>Date #{if @options.isLookup then \Lookup else \Integration}  Requested: #{@current.requested_timestamp}</p>"
					"<br/><br/>"
					"<p>Powered By Firmstep</p>"
				].join('\n')
		 
		opts.LIM = id: notification.LIM_id if notification.LIM_id

		console.info "['Integration'] Notification Options\n", opts if @debug and not @test
		
		error_log = @_createLog "['Notification Error']", true

		_final = (err) ~> 
			error_log := error_log
			error_log.error += " #{err?.message or err}"
			@logError error_log, true

		try new Notification opts .send! .caught (err) -> _final err
		catch err then _final err 

	_transform_profile: (profile={}, _profile={}) ->
		_.each profile.custom_values?.address?[profile.address_id], (v, i) ->
			_profile[i?.toLowerCase?!?.replace(/\_$/, '')] = v

		_.extend _profile, 
			title: profile.prefix
			suffix: profile.suffix
			gender: profile.gender
			country: profile.country

			name: profile.display_name
			sort_name: profile.sort_name
			last_name: profile.last_name
			first_name: profile.first_name

			contact_id: profile.contact_id
			birth_date: profile.birth_date
			contact_type: profile.contact_type
			organisation_name: profile.organisation_name
			ucrn: profile.custom_values?.Account_Information?[0]?.UCRN
			email_address: _.find(profile.email, is_primary: '1' )?.email
			status: profile.custom_values?.Account_Information?[0]?.Status
			alternative_email: _.find(profile.email, is_primary: '0' )?.email
			consent_to_share: profile.custom_values?.Additional_Details?.Consent_To_Share_
			preferred_contact_method: profile.custom_values?.Additional_Details?.Preferred_Contact_Method_name?.toLowerCase?!
			phone_number: _.find(profile.phone, is_primary: '1', phone_type_id: '1' )?.phone or _.find(profile.phone, phone_type_id: '1' )?.phone
			mobile_number: _.find(profile.phone, is_primary: '1', phone_type_id: '2' )?.phone or _.find(profile.phone, phone_type_id: '2' )?.phone
			alternative_number: _.find(profile.phone, is_primary: '1', phone_type_id: '3' )?.phone or _.find(profile.phone, phone_type_id: '3' )?.phone

	_xslt_transform: (body, temp={}) ->
		return body unless stylesheet = temp.xslt_stylesheet?.trim?!
		xslt.transform xslt.readXsltString(stylesheet), xslt.readXmlString(body), []

	_strip_namespaces: (body, temp={}) ->
		temp.strip_namespaces ?= '1'
		return body unless parseInt temp.strip_namespaces
		body?.replace /(<\/?)(\w+:)/g, "$1"

	_convertToDatabaseResponse: (data, temp, mode="XML") ->
		mode = mode?.toLowerCase?!
		xml = new libxmljs.Document!
		xmlDoc = libxmljs.parseXmlString data.trim?!
		body_path = if temp.version is '2' then "//WebserviceResult" else "//Body"

		if temp.version is '2' then body = xmlDoc.get(body_path)?.toString?!.trim?!
		else body = xmlDoc.get(body_path)?.text!.trim?!.replace? /\sxmlns[^"]+"[^"]+"/g, ""	
		throw new Error "No Webservice Body In Response: #{body_path} \n\n #{data}" unless body
		path_to_values = temp.repeatable_path or temp.path_to_values or '' or if mode is "xml" then body_path

		if mode is "json" 
			path_to_values ?= "//Root"
			body = json2xml Root: json_body = _.flatten [ JSON.parse body ]	

		body = @_xslt_transform @_strip_namespaces(body, temp), temp
		result = libxmljs.parseXmlString body, noblanks: true

		path = null; if path_to_values
			path = if path_to_values.charAt(0) isnt "/" then "//#{path_to_values}" else path_to_values
			result = result?.find path = path?.trim?!	

		temp.fields = {} if _.isString temp.fields
		if path and not _.size temp.fields then try
			for own i, v of result?[0]?.childNodes!
				name = v?.name?!; temp.fields[name] = name

		fields = xml
			.node 'Responses'
			.node 'RequestResponse'
			.node 'DatabaseResponse'
			.node 'Fields'

		for own i, v of temp.fields
			fields.node 'Field' .attr Type: \varchar, Name: v
		
		rows = fields.parent! .node 'Rows'
		for own n, r of result
			row = rows.node 'Row' .attr id: n?.toString?!
			for own i, v of temp.fields
				q = if r?.name?! is i then r?.text?! else r?.get?(i?.trim?!)?.text?!		
				row.node 'result', q .attr column: v, isNull: if q then \false else \true
		
		return [ xml, result, body ]

	_types:
		http: (temp) ->
			
			headers = []
			temp.responseType ?= 'xml'
			temp.url = temp.url?.trim?! or temp.url
			
			try temp.payload = JSON.parse temp.payload

			if temp.headers then for own i, h of temp.headers
				headers.push [ i?.trim?!, h?.trim?! ] 

			if _.isObject temp.payload
				temp.payloadType = 'JSON'
				temp.payload = JSON.stringify temp.payload 

			if temp.method?.toLowerCase! isnt 'get'
				headers.push [ 'Content-Length', Buffer.byteLength(temp.payload or '', 'utf8') ] 

			unless temp.url then hp = true
			else hp = temp.url.match /^(http|https|ftp)/ 
			unless hp then temp.url = "http://#{temp.url}"
			
			@start = process.hrtime!
			@current.limConnect.http temp.url, temp.payload, headers, temp.method, temp.payloadType
			.bind @ .then (data) ->
				@elapsed_time("http-lim-connect-#{@current.integration.id}") if @debug_runtimes
				return data if temp.noConversion

				[ xml, result, body ] = @_convertToDatabaseResponse data, temp, temp.responseType

				if @debug and not @test
					console.info "['Integration'] HTTP Result\n", result.toString?!
					console.info "['Integration'] HTTP Response XML\n", xml?.toString?!
					console.info "['Integration'] HTTP Response Post Transform XML\n", body?.toString?!
					console.info "['Integration'] HTTP Response Pre Transform XML\n", @current.limConnect._response

				@elapsed_time("http-lim-connect-#{@current.integration.id}") if @debug_runtimes
				@current.limConnect.pre_transform = @current.limConnect._response
				@current.limConnect.post_transform = body?.toString?!
				@current.limConnect._response = xml?.toString?!

		webservice: (temp) ->
			return @_types['webservice_http'].call @, temp if not temp.version or temp.version is '1'

			temp.options = 
				endpoint: temp.endpoint
				wsdl: temp.wsdl or temp.link
				function: temp.function or temp.SOAPAction
				auth: type: temp.auth_type, method: temp.auth_method
				auth_values: Username: temp.auth_user, Password: temp.auth_pass, Token: temp.auth_token

			@start = process.hrtime!
			@current.limConnect.response_type = \json unless temp.command is \Invoke
			@current.limConnect.webservice temp.command, temp.options, temp.payload
			.bind @ .then (data) -> 
				@elapsed_time("webservice-lim-connect-#{@current.integration.id}") if @debug_runtimes
				try JSON.parse data catch e then data if @current.limConnect.response_type is \json
				return data if temp.noConversion or temp.command isnt \Invoke

				[ xml, result, body ] = @_convertToDatabaseResponse data, temp

				if @debug and not @test
					console.info "['Integration'] Web Service Result\n", result.toString?!
					console.info "['Integration'] Web Service Response XML\n", xml?.toString?!
					console.info "['Integration'] Web Service Post Transform XML\n", body?.toString?!
					console.info "['Integration'] Web Service Response Pre Transform XML\n", @current.limConnect._response

				@elapsed_time("webservice-lim-connect-#{@current.integration.id}") if @debug_runtimes
				@current.limConnect.pre_transform = @current.limConnect._response
				@current.limConnect.post_transform = body?.toString?!
				@current.limConnect._response = xml?.toString?!

		webservice_http: (temp) ->

			if temp.auth_user and temp.auth_pass
				temp.Authorization = utils.toBase64 "#{temp.auth_user}:#{temp.auth_pass}"
			else if temp.auth_token then temp.Authorization = utils.toBase64 temp.auth_token

			headers =    [ [ 'Content-Type', 'text/xml' ] ]
			headers.push [ 'SOAPAction', temp.SOAPAction ] if temp.SOAPAction
			headers.push [ 'Authorization', temp.Authorization ] if temp.Authorization
			headers.push [ 'Content-Length', Buffer.byteLength(temp.payload or '', 'utf8') ] 

			unless temp.endpoint
				temp.scheme = if temp.port is '443' then 'https://' else 'http://'
				temp.endpoint = "#{temp.scheme}#{temp.server}#{if temp.port then ":#{temp.port}" else ""}#{temp.path}"

			@start = process.hrtime!
			@current.limConnect.webservice_http temp.endpoint, temp.payload, headers
			.bind @ .then (data) ->
				[ xml, result, body ] = @_convertToDatabaseResponse data, temp

				if @debug and not @test
					console.info "['Integration'] Web Service Result\n", result.toString?!
					console.info "['Integration'] Web Service Response XML\n", xml?.toString?!
					console.info "['Integration'] Web Service Post Transform XML\n", body?.toString?!
					console.info "['Integration'] Web Service Response Pre Transform XML\n", @current.limConnect._response

				@elapsed_time("webservice-lim-connect-#{@current.integration.id}") if @debug_runtimes
				@current.limConnect.pre_transform = @current.limConnect._response
				@current.limConnect.post_transform = body?.toString?!
				@current.limConnect._response = xml?.toString?!

		database: (temp) ->

			@start = process.hrtime!
			@api.get "/connection-strings", { action: 'get', id: temp.systemID } .bind @ 

			.then (conn) -> 
				@elapsed_time("get-connection-string-#{@current.integration.id}")
				return conn unless _.isEmpty conn?.Value
				throw new Error "No Connection String"

			.then (conn) ->
				conn_str = conn.Value
				driver = conn.Connection_Type or 'ODBC'

				console.info "['Integration'] Database Connection String\n", driver, '-', conn_str, if @debug and not @test
				
				parameters = temp.calc_query?[1] or []
				query = temp.calc_query?[0] or temp.query

				for own x, y of parameters
					p = null; try p = JSON.parse y?.value
					parameters[x].value = p.content if p?.content

				@start = process.hrtime!
				@current.limConnect.sql driver, conn_str, query, parameters
				.bind @ .then -> @elapsed_time("database-lim-connect-#{@current.integration.id}") if @debug_runtimes; it

		email: (temp) ->

			isHtml = 'true' # (temp.isHtml in ['Yes', 'yes', 'true', true]).toString?!
			subject = temp.subject
			_from = [ temp.from, temp.fromName ]

			body = { message: temp.body?.trim?!, html: isHtml }
			priority = if temp.priority then parseInt temp.priority

			recipients = []
			if temp.recipients_to?.trim?! then for r in temp.recipients_to.split(',')
				( recipients.push ['', r.trim?!, 'To' ] if r = r?.trim?! )
			if temp.recipients_cc?.trim?! then for r in temp.recipients_cc.split(',')
				( recipients.push ['', r.trim?!, 'CC' ] if r = r?.trim?! )
			if temp.recipients_bcc?.trim?! then for r in temp.recipients_bcc.split(',')
				( recipients.push ['', r.trim?!, 'Bcc' ] if r = r?.trim?! )

			_attachments = []
			try if _.isString temp.attachments
				try temp.attachments = JSON.parse temp.attachments
				for a in temp.attachments.split('|')
					try a = JSON.parse a; continue unless a
					unless _.isArray a then _attachments.push a
					else for i in a then _attachments.push i
				temp.attachments = _attachments

			attachments = []
			try if temp.attachments
				temp.attachments = [ temp.attachments ] unless _.isArray temp.attachments 
				for r in temp.attachments
					try r = JSON.parse r
					if r?.content and r?.name and r?.type
						attachments.push [ r.name, r.type, r.content, r.isEncoded ] 

			try if temp.pdf_attachments
				temp.attachments = [ temp.attachments ] unless _.isArray temp.attachments 
				for r in temp.pdf_attachments
					try r = JSON.parse r
					if r?.content and r?.name and r?.type
						attachments.push [ r.name, r.type, r.content, r.isEncoded ] 	
			
			@start = process.hrtime!
			@current.limConnect.email recipients, _from, subject, body, attachments, priority
			.bind @ .then -> @elapsed_time("email-lim-connect-#{@current.integration.id}") if @debug_runtimes; it

		file: (temp) ->

			Promise.resolve true .bind @

			.then ->
				throw new Error "No File Content" unless temp.content
				throw new Error "No Write Mode" unless mode = settings.file_modes[temp.mode or temp.writeMode]
				return mode
			
			.then (mode) ->

				tasks = []; ret = []
				try temp.content = JSON.parse temp.content
				temp.content = _.flatten [ temp.content ]

				@start = process.hrtime!
				for own x, file of temp.content
					continue unless file
					try file = JSON.parse file

					name = file.name or ''
					temp.destination ?= ''
					content = file.content or file

					throw new Error "No Destination" unless destination = "#{temp.destination}/#{name}"?.replace /^\//, '' ?.replace /\/$/, ''

					tasks.push ( @current.limConnect.files do
						temp.operation, temp.root, destination, content, mode, file.isEncoded
					.then (res) -> ret.push res )

				Promise.all tasks .bind @ .then -> ret
				
				.then -> @elapsed_time("file-lim-connect-#{@current.integration.id}") if @debug_runtimes; it
			
		profile: (temp) ->

			@start = process.hrtime!

			@current.limConnect._request = profile_args = temp

			Promise.resolve yes .bind @

			.then ->
				options = _.extend { action: temp.profile_action }, @options
				p = new Profile options, @debug, @test
				p.run profile_args

			.then -> @elapsed_time("profile-api-connect-#{@current.integration.id}") if @debug_runtimes; it

			.then -> 
				@current.limConnect._duration = @runtimes["profile-api-connect-#{@current.integration.id}"]?.replace? /ms$/, ''
				@current.limConnect._response = it; throw new Error it.error if it?.error
				@current.limConnect.pre_transform = @current.limConnect._response
				@current.limConnect.post_transform = @_transform_profile it
				return it
		
		permission: (temp) ->

			@start = process.hrtime!

			@current.limConnect._request = perm_args = temp

			Promise.resolve yes .bind @

			.then ->
				options = _.extend { action: temp.permission_action }, @options
				p = new Permission options, @debug, @test
				p.run perm_args

			.then -> @elapsed_time("permission-api-connect-#{@current.integration.id}") if @debug_runtimes; it

			.then -> 
				@current.limConnect._duration = @runtimes["permission-api-connect-#{@current.integration.id}"]?.replace? /ms$/, ''
				@current.limConnect._response = it?[0] or it; throw new Error it.error if it?.error
				@current.limConnect.pre_transform = @current.limConnect._response
				@current.limConnect.post_transform = it?[1] or it?[0] or it;
				return it

		printable: (temp) ->

			style = settings.style

			_message = [
				"<html><head>"
				# "<link rel='stylesheet' type='text/css' href='http://localhost/fillform/styles/AchieveForms.min.css'/>"
				"<style type='text/css'> #{style} </style>"
				"</head><body> #{temp.content} </body></html>"
			].join '\n'

			@current.limConnect._request = pdf_args = 
				name: "#{(temp.name or temp.filename or 'Printable')}.pdf"
				html: utils.toBase64 _message

			@start = process.hrtime!
			@api.post "/pdf-generator", null, pdf_args, false .bind @

			.then -> @elapsed_time("printable-api-connect-#{@current.integration.id}") if @debug_runtimes; it
			
			.then -> 
				@current.limConnect._duration = @runtimes["printable-api-connect-#{@current.integration.id}"]?.replace? /ms$/, ''
				@current.limConnect._response = it; throw new Error it.error if it?.error
				it?.isEncoded = true
				return it

		case: (temp) ->

			@current.limConnect._request = task_args =
				strict: "0"
				createCase: "1"
				case_status: "1"
				task_status: "1"
				subscribable: "1"
				data_visibility: "1"
				data: temp.process_data
				process_id: temp.process_id
				process_name: temp.process_name
				aggregate_data: temp.process_data
				assigned_user_id: temp.assigned_user_id
				assigned_group_id: temp.assigned_group_id
				task_id: @options.global_tokens.reference

				stage_form: temp.stage_form
				stage_id: temp.stage_id or temp.process_id
				stage_name: temp.stage_name or temp.process_name 

				date_started: temp.date_started
				date_escalation: temp.date_escalation
				escalation_status: temp.escalation_status
				escalation_user_id: temp.escalation_user_id
				escalation_group_id: temp.escalation_group_id

				citizen_id: temp.citizen_id
				citizen_name: temp.citizen_name

				published: temp.published
				view_group_id: temp.view_group_id

			@start = process.hrtime!
			@api.post "/createTask", null, task_args .bind @

			.then -> @elapsed_time("case-api-connect-#{@current.integration.id}") if @debug_runtimes; it

			.then ->
				@current.limConnect._duration = @runtimes["case-api-connect-#{@current.integration.id}"]?.replace? /ms$/, ''
				@current.limConnect._response = it; throw new Error it.error if it?.error
				return it
			
		payment: (temp) ->
			Promise.resolve true .bind @

			.then -> throw new Error "Payments can only be run as a pre-submission integration"

