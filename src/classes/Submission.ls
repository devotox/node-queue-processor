require! {
	fs: \fs
	_: \lodash
	util: \util
	uuid: \node-uuid
	bugsnag: \bugsnag
	Promise: \bluebird
	json2xml: \json2xml
}


API = require "./Api"
require "./../libs/date"
utils = require "./utils"
Process = require "./Process"
Validator = require "./Validator"
Integration = require "./Integration"
Notification = require "./Notification"

module.exports = class Submission

	settings =
		notify: yes
		notification:
			message: ''
			subject: "New AF Submission Error"
			recipients: <[ devonte.emokpae@firmstep.com ]>
		ignore_errors: <[ NoSubmitDocumentError NoFormError ]>

	defaults = 	
		print_data: no
		max_retries: 5
		customer_id: null
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

		forms_endpoint: 'sandbox://'
		files_endpoint: 'sandbox-files://'
		publish_endpoint: 'sandbox-publish://'
		process_endpoint: 'sandbox-processes://'
		submission_endpoint: 'sandbox-submissions://'

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>
	
		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Submission Options \n', options

		@options = _.defaults options, defaults

		throw new Error \NoCustomerIdError unless @options.customer_id
		throw new Error \NoFormsEndpointError unless @options.forms_endpoint
		throw new Error \NoFilesEndpointError unless @options.files_endpoint
		throw new Error \NoSubmissionEndpointError unless @options.submission_endpoint

		@delete = _.bind @delete, @
		@api = new API @options, @debug, @test

	_getSubmitDocument: (uri, retries=0) ->

		Promise.resolve true .bind @

		.then -> @api.docapi uri

		.then ->
			throw new Error \NoSubmitDocumentError unless it?.content			
			console.info "['Submission'] Temp Submit Document Retrieved"
			_.extend it?.content, created: new Date!.toString "yyyy-MM-dd HH:mm:ss"
			_.extend it?.metadata, created: new Date!.toString "yyyy-MM-dd HH:mm:ss"
			return it

		.caught -> 
			retries := parseInt(retries) or 0
			throw new Error \NoSubmitDocumentError if retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @_getSubmitDocument uri, retries

	_getForm: (uri=@submission.formUri, retries) ->
		
		Promise.resolve true .bind @

		.then -> @api.docapi uri

		.then -> 
			throw new Error \NoFormError unless it?.content
			console.info "['Submission'] Form Definition Retrieved"
			return @_form = it

		.caught -> 
			retries := parseInt(retries) or 0
			throw new Error \NoFormError if retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @_getForm uri, retries

	_getProcess: (uri=@submission.processUri, retries) ->
		return @_process = null unless @submission.processDef
		
		Promise.resolve true .bind @

		.then -> @api.docapi uri

		.then -> 
			throw new Error \NoProcessError unless it?.content
			console.info "['Submission'] Process Definition Retrieved"
			return @_process = it

		.caught -> 
			retries := parseInt(retries) or 0
			throw new Error \NoProcessError if retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @_getProcess uri, retries

	_getEnvTokens: (retries) ->

		Promise.resolve true .bind @

		.then -> @api.get "/environment-tokens" 

		.then -> 
			env_tokens = {}
			throw new Error \NoEnvTokensError unless it
			for own x, y of it then env_tokens[y.Name] = y.Value 
			console.info "['Submission'] Environment Tokens Retrieved"
			@_submission.env_tokens = _.cloneDeep @submission.env_tokens = env_tokens
		
		.caught -> 
			retries := parseInt(retries) or 0
			throw new Error \NoEnvTokensError if retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @_getEnvTokens retries

	_setData: (data, doc) ->
		throw new Error 'No Submission Form Document' unless doc

		@sdata = data
		@csa = data.csa
		@user = data.user
		@temp_uri = data.uri
		@submission = doc.content
		@reference = data.reference
		@_submission = _.cloneDeep @submission

		@submission.summary = @_getSummary @submission
		@_submission.summary = _.cloneDeep @submission.summary
		@transaction_reference = @_submission.transactionReference
		
		@_global_tokens = @_submission.global_tokens = 
			reference: @reference
			created: @_submission.created
			_customer_: @options.customer_id
			_summary_: @_submission.summary_fields
			payment_reference: @_submission.transactionReference
			transaction_reference: @_submission.transactionReference			
			xml_data: json2xml Submission: @_submission.formValues
			json_data: JSON.stringify Submission: @_submission.formValues

	_getSummary: (submission=@submission) ->
		return unless values = submission?.formValues
		summary = []
		_.each values, (section) ->
			return true unless _.size section
			_.each section, (field) ->
				return true unless field?.isSummary
				summary.push do
					name: field.name
					label: field.label
					value: field.value
					summary: field.summary
					value_label: field.value_label
		return summary

	_setCategory: ->
		@category = @_process?.metadata?.category or @_form?.metadata?.category

	_validate: ->
		do_validate = ~>
			args =			
				form: @_form
				process: @_process
				submission: @_submission
				customer_id: @options.customer_id
			
			new Validator args, @debug, @test 

			.validate! .bind @

			.then -> console.info "['Submission'] Form Data Valid!"

			.caught -> @notify it, yes #; throw new Error (it?.message or it)

		try do_validate!
		catch err then @notify err, yes #; throw new Error (err?.message or err)

	submit: (data) ->
		data = { uri: data } if _.isString data
		throw new Error \NoTempUriError unless data?.uri
		throw new Error \NoUserError unless data?.user and not _.isEmpty data?.user

		Promise.delay 3000 .bind @	

		.then -> @_getSubmitDocument data.uri 

		.then -> @_setData data, it

		.then -> @_getEnvTokens!

		.then -> @_getProcess!

		.then -> @_getForm!

		.then -> @_setCategory!

		.then -> @_validate!

		.then -> @_run!	

		.caught -> 
			@notify it, yes unless it?.message in settings.ignore_errors
			throw new Error (it?.message or it)

	_run: ->
		Promise.resolve true .bind @		

		.then -> @process!

		.then -> @integrations!

		.then -> @printSubmissions!

		.caught -> @notify it, yes

		.then -> @moveToStore!

	notify: (message, isError, notification=settings.notification) ->
		return unless settings.notify; @submission or= {}

		recipients = _.map notification.recipients, (email) -> return [ '', email?.trim?(), 'To' ]
		if recipients?.length then recipients = _.filter recipients, (rec) -> return rec?[1] and rec?[2]
		return unless recipients?.length

		message = message?.message or message
		_from = if isError then \errors@firmstep.com else \notifications@firmstep.com

		opts = 
			API: 
				docapi_url: @options.docapi_url
				customer_id: @options.customer_id
				apibroker_url: @options.apibroker_url

			template: 
				html: true, type: \email, recipients: recipients, from: [ _from, \Firmstep ]
				subject: "#{notification.subject or 'Firmstep Notification'} - " + @reference
				message: [
					"#{notification.message or ''}"
					"<pre>#{if isError then 'Error: ' else 'Message: '}#{message}</pre>"
					"<p>Form: #{@submission.formName}</p>"
					"<p>Form ID: #{@submission.formId}</p>"
					"<p>Site: #{@submission.tokens?.site_url}</p>"

					"<p>Process: #{@submission.processDef?.processName or 'None'}</p>"
					"<p>Process ID: #{@submission.processDef?.props.id or 'None'}</p>"

					"<p>Stage: #{@submission.stage_name or 'None'}</p>"
					"<p>Stage ID: #{@submission.stage_id or 'None'}</p>"

					"<p>Case ID/Reference: #{@submission.case_id or 'None'}</p>"
					"<p>Submission Reference: #{@reference}</p>"

					"<p>Transaction Reference: #{@transaction_reference or 'none'}</p>"
					"<p>Temporary Submission URI: #{@temp_uri}</p>"

					"<p>User Email: #{@user?.email or 'None'}</p>"
					"<p>User Name: #{@user?.name or 'None'}</p>"
					"<p>User ID: #{@user?.id or 'None'}</p>"

					"<p>CSA Email: #{@csa?.email or 'None'}</p>"
					"<p>CSA Name: #{@csa?.name or 'None'}</p>"
					"<p>CSA ID: #{@csa?.id or 'None'}</p>"

					"<p>Submitter Email: #{@submission.user?.email or 'None'}</p>"
					"<p>Submitter User-Agent: #{@submission.tokens?.user_agent or 'None'}</p>"

					"<p>Date Submitted: #{@submission.dateCreated} #{@submission.timeCreated}</p>"
				].join '\n'

		bugsnag.notify new Error(message), data: @submission, errorName: "['Submission']" if isError

		opts.LIM = id: notification.LIM_id if notification.LIM_id
		new Notification opts .send!

	process: ->
		return unless @submission.processDef

		@_submission.current_stage = _.find @_submission.processDef.stages, (v, i) ~>
			return v if v.id is @_submission.stage_id or v.name is @_submission.stage_name

		args = 
			csa: @csa
			citizen: @user
			category: @category
			reference: @reference
			submission: @_submission
			process: @_submission.processDef
			customer_id: @options.customer_id
			stage: @_submission.current_stage
			global_tokens: @_submission.global_tokens
			
		new Process args, @debug, @test .run!

	integrations: ->
		return if @submission.processDef
		return unless @submission.integrations.length

		args = 
			submission: @_submission
			customer_id: @options.customer_id
			integration: @_submission.integrations
			global_tokens:  @_submission.global_tokens

		new Integration args, @debug, @test .run!

	printSubmissions: ->		
		return unless @options.print_data
		fs.writeFile "submission.#{@reference}.json", JSON.stringify @submission.formValues, null, 4
		fs.writeFile "submission.#{@reference}.edited.json", JSON.stringify @_submission.formValues, null, 4

	moveToStore: ->
		throw new Error \NoUserError unless @sdata?.user?.id

		return [ @reference ] if @submission.saveSubmissionData in [ false, \false ]

		args = 
			metadata: 
				'id': @reference
				'user-id': @sdata.user.id
				'created': @submission.created
				'form-name': @submission.formName
				'user-display-name': @sdata.user.name
				'form-category': @_form.metadata.category
				'user-agent': @submission.tokens?.user_agent
				'transaction-reference': @transaction_reference
				'form-uri': @options.forms_endpoint + @submission.formId
				'isPublished': if @submission.isPublished then '1' else '0'
	
		if _.isObject @submission.processDef
			_.extend args.metadata, 
				'stage-id':  @submission.stage_id
				'stage-name': @submission.stage_name
				'publish-form-id': @submission.publish_id
				'process-id': @submission.processDef.props.id
				'process-name': @submission.processDef.processName

		if @submission.isPublished and args.metadata['form-uri'] isnt @submission.formUri
			args.metadata['publish-form-uri'] = @submission.formUri

		if @csa?.id then _.extend args.metadata,
			'csa-display-name': @csa.name
			'csa-email': @csa.email
			'csa-id': @csa.id

		_.each <[ tokens env_tokens ]>, (v, i) ~> delete @submission[i]
		submission_uri = @options.submission_endpoint + "AF-Submission-#{uuid.v4!}"

		args.content = utils.toBase64 JSON.stringify @submission

		@api.docapi submission_uri, 'put', args .bind @

		.then -> [ @reference, submission_uri ]

	delete: -> 
		unless @temp_uri
			console.info "['Submission'] No Temp Submission File To Delete"
			return Promise.resolve false 

		console.info "['Submission'] Deleting Temp Submission File @ #{@temp_uri}"
		@api?.docapi? @temp_uri, \delete


