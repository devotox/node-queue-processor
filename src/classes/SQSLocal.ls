
require! {
	_: \lodash
	events: \events
	Promise: \bluebird
}

root = "../"
API = require "./Api"
require "#{root}libs/date"

module.exports = class SQSLocal extends events.EventEmitter

	defaults = 
		debug: false
		concurrency: 1
		waitTimeSeconds: 20
		visibilityTimeout: 300
		maxNumberOfMessages: 1

		customer_id: null
		sqs_url: \http://localhost/sqs/
		docapi_url: \http://localhost/docapi/
		apibroker_url: \http://localhost/apibroker/

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'SQS LOCAL Options \n', options
			_.extend options, debug: @debug

		@options = _.defaults options, defaults

		@api = new API @options, @debug, @test, true
		@run!

	run: ->
		Promise.resolve true .bind @

		.then -> @recieveMessage!

		.then -> @emitMessage it

		.lastly -> @run!

		#.lastly -> Promise.delay @options.waitTimeSeconds * 1000 .bind @ .then @run

	sendEvent: (e, data) -> @emit e, data

	recieveMessage: -> @api.sqs 'receive'

	sendMessage: (message, cb) ->
		@api.sqs 'queue', null, message .then cb

	deleteMessage: (handle, cb) -> 
		@api.sqs 'delete', null, ReceiptHandle: handle .then cb

	emitMessage: (message) ->
		if message then @sendEvent \message, 
			type: \message, message: message
			data: JSON.parse(message.Body) or message.Body
			deleteMessage: (cb) ~> @deleteMessage message.ReceiptHandle, cb
