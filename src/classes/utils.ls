require! {
	html_entities: \he
	libxmljs: \libxmljs
}

utils = module.exports = {}

utils.ucfirst = (str='') -> 
	[ str.charAt 0 .toUpperCase!, str.substr 1 ].join ''

utils.fromBase64 = (str='') ->
	return str unless str
	return (new Buffer(str or "", "base64")).toString 'utf8'
	
	try 
		str = (new Buffer(str or "", "base64")).toString 'utf8'
		decodeURIComponent escape str
	catch e then (new Buffer(str or "", "base64")).toString 'utf8'

utils.toBase64 = (str='') ->
	return str unless str
	return (new Buffer(str or "", 'utf8')).toString 'base64'
	
	try 
		str = unescape encodeURIComponent
		(new Buffer(str or "", 'utf8')).toString 'base64'
	catch e then (new Buffer(str or "", 'utf8')).toString 'base64'

utils.uuid = (len=6) ->
	uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace /[xy]/g, (c) ->
		r = Math.random() * 16 .|. 0
		v = (if c is "x" then r else r .&. 0x3 .|. 0x8)
		v.toString 16
	return uuid unless len
	uuid.replace(/\-/g, "").substring 0, len

utils.guid = (len=13) ->
	@uuid(len)?.replace? /\-/g, ""

utils.nl2br = (str='') ->
	str?.replace? /\n/gm, "<br />"

utils.br2nl = (str='') ->
	str?.replace? /<br\s*\/?>/gmi, "\n"

utils.stripBraces = (value='') ->
	value?.toString?!
	.replace(/\{/gmi, '&#123')
	.replace(/\|/gmi, '&#124')
	.replace(/\}/gmi, '&#125')
	.replace(/\~/gmi, '&#126')
	.replace(/(\\)/gmi, '&#92;')
	.replace(/\£/gmi, '&pound;')

utils.stripTags = (value='') -> 
	value?.toString?!
	.replace? /(<([^>]+)>)/gmi, ''

utils.preEncode = (value='') ->
	value?.toString?!
	.replace? /(\\")/g, '"'

utils.replaceEntities = (value='', html_in_value, xml_in_value) ->
	allow = if html_in_value then true else false
	html_entities.encode utils.preEncode(value),
		allowUnsafeSymbols: allow
		useNamedReferences: true

utils.xmlEncode = (value='', html_in_value, xml_in_value) ->
	value = utils.replaceEntities value, html_in_value, xml_in_value
	value = utils.stripTags value unless html_in_value or xml_in_value
	value = utils.stripBraces value
	return value

utils.xmlDecode = (value='') ->
	html_entities.decode value

