require! {
	_: \lodash
	Promise: \bluebird
}

API = require "./Api"
utils = require "./utils"

module.exports = class Publisher

	defaults = 		
		type: null
		delay: 3000
		max_retries: 5
		customer_id: null

		form_endpoint: "sandbox://"
		process_endpoint: "sandbox-processes://"

		publish_endpoint: "sandbox-publish://"
		archive_endpoint: "sandbox-archive://"

		definition_name: "definition.json"
		docapi_url: "http://localhost/docapi/"
		apibroker_url: "http://localhost/apibroker/"

	(options={}, @debug=process.env.DEBUG, @test=process.env.TEST) ~>

		if @debug and process.env.SHOW_OPTIONS and not @test and not _.isEmpty options 
			console.info 'Publisher Options \n', options

		@options = _.defaults options, defaults
		
		throw new Error \NoCustomerIdError unless @options.customer_id
		throw new Error \NoDocumentTypeError unless @type = @options.type
		throw new Error \NoFormEndpointError unless @options.form_endpoint
		throw new Error \NoProcessEndpointError unless @options.process_endpoint
		throw new Error \NoPublishEndpointError unless @options.publish_endpoint
		throw new Error \NoArchiveEndpointError unless @options.archive_endpoint

		@delete = _.bind @delete, @
		@api = new API @options, @debug, @test

	createUri: (path) ->
		[ @options.publish_endpoint, path, '/', @options.definition_name ].join('')

	run: (data) ->
		data = { uri: data } if _.isString data
		data.action ?= 'publish'
		@publish data

	publish: (data) ->

		data = { uri: data } if _.isString data; data.action ?= 'publish'

		throw new Error \NoTempUriError unless @temp_uri = data?.uri

		Promise.delay @options.delay .bind @

		.then -> @getPublishDocument data.uri

		.spread (publish_uri, doc) ->
			@main_doc = doc.content
			@cwd = doc.content.props.id

			if @debug and not @test
				console.info "['Publisher'] #{utils.ucfirst data.action}ing #{@type} #{doc.content.formName or doc.content.processName} @ #{publish_uri}"
			
			@exists publish_uri .bind @ 
			.then (ex) ->
				d = if data.action is 'publish' then doc?.content else ex?.content or doc?.content
				if ex then @archive ex, data .bind @ .then ->
					@["_#{data.action}"] publish_uri, d, data, @cwd
				else @["_#{data.action}"] publish_uri, d, data, @cwd

		.then -> [ @main_doc?.formName or @main_doc?.processName, it ]

	_publish: (uri, doc, data) ->
		tasks = []
		tasks.push @_addDefinition uri, doc, data
		tasks.push @_publishSubforms doc, @cwd if @type is \form
		tasks.push @_publishProcessForms uri, doc, @cwd if @type is \process

		Promise.all tasks .then -> it?[0]

	_unpublish: (uri, doc, data, cwd) ->

		sub_unpublish = (s) ~>
			_cwd = "#{cwd}/#{s.id}"
			@getPublishDocument( _uri = @createUri _cwd )
			.bind @ .spread (i, doc) -> @_unpublish _uri, doc.content, data, _cwd

		Promise.resolve yes .bind @

		.then -> 
			isProcess = doc?.stages; isForm = doc?.sections
			doc_type = if isForm then 'Form' else 'Process'
			return unless isProcess or isForm; tasks = []
			d = isForm or isProcess

			console.info "['Publisher'] Unpublishing #{doc_type}: #{doc?.formName or doc?.processName}"
				
			_.each d, (s) ~>
				return tasks.push sub_unpublish { id: s.id } if isProcess 
				_.each s.fields, (field) ~>
					return true unless field.type is 'subform' and id = field.props.form 
					tasks.push sub_unpublish { id: id }

			ret = [ uri ]
			_remove = ~>
				Promise.all tasks .bind @
				.then -> ret := ret.concat _.flatten it
				.then -> @api.docapi uri, 'delete'
				.then -> ret

			@api.docapi uri .bind @ 
			.then -> return ret unless _.size it.metadata; _remove()

	getPublishDocument: (uri, retries=0) ->

		Promise.resolve true .bind @

		.then -> @api.docapi uri

		.then (doc) ->
			throw new Error \NoTempDocError unless doc?.content?.props
			publish_uri = @createUri doc.content.props.id
			[ publish_uri, doc ]

		.caught -> 
			retries := parseInt(retries) or 0
			throw new Error \NoPublishDocumentError if retries >= @options.max_retries
			Promise.delay Math.exp(2, ++retries) * 1000 .bind @ .then -> @getPublishDocument uri, retries

	_addDefinition: (uri, doc, data, addMeta=true, meta={}, type=@type) ->
		args = 
			force: 1, metadata: meta or {}
			content: utils.toBase64 JSON.stringify doc		
		
		doc.props.publish_id = uri.split('://')[1]		
		_.extend args.metadata, @_addMetadata uri, doc, data, type if addMeta
		
		if meta?['stage-id'] and addMeta	
			args.metadata['form-category'] = args.metadata.category
			args.metadata.category = meta?['process-category'] or ''

		console.info "['Publisher'] Adding Definition @ #{uri}" if @debug and not @test

		@_getLanguageNames doc, args .bind @ .then -> @api.docapi uri, \put, args

	_getLanguageNames: (doc, args) ->
		return Promise.resolve true unless _.size doc.props.languages

		tasks = [];  _.each doc.props.languages, (lang_id, key) ~>
			tasks.push ( @api.docapi @options.form_endpoint + lang_id .bind @ .then ->
				name = null; it?.content?.replace? /formName,\s*(.*)/gm, (a, b) -> name := b
				args.metadata["form-name-#{key}"] = name if name
			)
		return Promise.all tasks

	_addMetadata: (uri, doc, data, type=@type) ->
		meta = 
			"type": type
			"publish-uri": uri
			# "languages": doc.props.languages

		if _.isObject data then _.extend meta, 
			"published-by-id": data.user?.id
			"published-by-name": data.user?.name

		if type is "form" then _.extend meta, 
			"form-id": doc.props.id
			"form-name": doc.formName
			"category": doc.props.category
			"form-description": doc.formDescription
			"form-uri": doc.props.uri or doc.props.id
			"submission-api": doc.props.submission?.api
			
		else if type is "process" then _.extend meta, 
			"process-id": doc.props.id
			"category": doc.props.category
			"process-name": doc.processName
			"initial-stage": doc.props.initial_stage
			"process-description": doc.processDescription
			"initial-stage-id": doc.props.initial_stage_id
			"process-uri": @options.process_endpoint + doc.props.id

			/*
			stages_uri = {}; _.each doc.stages, (stage, stage_name) ~>
				stage_uri = [stage.id, @options.definition_name].join('/')
				stages_uri[stage_name] = uri.replace @options.definition_name, stage_uri
			meta[\stages-uri] = JSON.stringify stages_uri
			*/

		return meta

	_createSubform: (subform, cwd, form, field) ->
		throw new Error "['Create Subform'] No Subform @ Field: #{field.props.dataName} within Form: #{form.formName}" unless subform.props

		path = "#{cwd}/#{subform.props.id}"
		publish_uri = @createUri path
		formName = subform.formName

		console.info "['Publisher'] Publishing Subform #{formName} @ #{publish_uri}" if @debug and not @test

		Promise.all [
			@_addDefinition publish_uri, subform, null, false
			@_publishSubforms subform, path
		]
		.then (data) -> console.info "['Publisher'] Subform #{formName} Published!"; return data

	_publishSubforms: (doc, path) ->
		tasks = []; return Promise.resolve() unless _.size doc?.sections

		_.each doc.sections, (section) ~>
			return true unless _.size section.fields
			_.each section.fields, (field) ~>
				return true unless field.type is "subform" and field.props.form
				tasks.push ( @api.docapi @options.form_endpoint + field.props.form .bind @ .then (subform) -> 
					@_createSubform subform.content, path, doc, field )
		
		Promise.all tasks

	_createStageForm: (uri, doc, stage, process, cwd) ->
		path = "#{cwd}/#{stage.id}"
		publish_uri = @createUri path

		stage_meta = 
			"stage-id": stage.id
			"stage-name": stage.name
			"process-publish-uri": uri
			"process-id": process.props.id
			"process-name": process.processName
			"process-category": process.props.category
			"process-description": process.processDescription
			"process-uri": @options.process_endpoint + process.props.id

		stage_meta = null unless stage.name is process.props.initial_stage
		addMeta = if stage_meta then true else false
		formName = doc.formName

		console.info "['Publisher'] Publishing Stage Form #{formName} @ #{publish_uri}" if @debug and not @test

		Promise.all [
			@_addDefinition publish_uri, doc, null, addMeta, stage_meta, "form"
			@_publishSubforms doc, path
		]
		.then (data) -> console.info "['Publisher'] Stage Form #{formName} Published!"; return data

	_publishProcessForms: (uri, doc, path) ->
		tasks = []; return Promise.resolve() unless _.size doc?.stages

		_.each doc.stages, (stage, name) ~>
			return true unless stage.props.form
			tasks.push ( @api.docapi @options.form_endpoint + stage.props.form .bind @ .then (form) ->
				@_createStageForm uri, form.content, stage, doc, path )
		
		Promise.all tasks

	isError: (txt) ->
		txt.indexOf(\<Error>) isnt -1

	exists: (uri) ->
		@api.docapi uri .bind @ 
		.then (data) ->			
			return false if _.isString(data.content) and @isError(data.content)
			return data
		.caught -> return false

	archive: (doc, data={}) ->
		uuid = require \node-uuid
		archive_uri = @options.archive_endpoint + "AF-Archive-#{uuid.v4!}"
		
		console.info "['Publisher'] Archiving #{@type} #{doc.content.formName or doc.content.processName} @ #{archive_uri}"
		
		_.extend doc.metadata, 
			"archive-uri": archive_uri
			"archived-by-id": data?.user?.id
			"archived-by-name": data?.user?.name

		@api.docapi archive_uri, \put, 
			content: utils.toBase64 JSON.stringify doc.content
			metadata: doc.metadata
			force: 1

	delete: ->
		unless @temp_uri
			console.info "['Publisher'] No Temp Publish File To Delete"
			return Promise.resolve false 
			
		console.info "['Publisher'] Deleting Temp Publish File @ #{@temp_uri}"
		@api?.docapi? @temp_uri, \delete

