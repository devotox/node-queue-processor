require! {
	_: \lodash
	Promise: \bluebird
}

root = "../../"
API = require "#{root}build/src/classes/Api"
DataDump = require "#{root}build/src/classes/DataDump"

{ArgumentParser} = require \argparse

parser = new ArgumentParser do
	description: "FS-Node Data Dump"
	addHelp: yes

parser.addArgument <[ -d --debug ]>,
	help: 'Set node to debug mode.'
	required: no

parser.addArgument <[ -c --customers ]>,
	help: 'Set Customer IDs.'
	required: no

console_args = parser.parseArgs!
process.env.DEBUG = yes if console_args.debug

try customers = JSON.parse console_args.customers
catch e then customers = [ console_args.customers ] if console_args.customers

run = (customers) ->	
	tasks = []; _.each customers, (c) ->
		try d = new DataDump do
			query_opts: c.query_opts
			customer_id: c.customer_id
			client: c.client or= \mssql
		tasks.push d.run! if d
	Promise.all tasks

return run customers if _.size customers

api = new API customer_id: "00000000-0000-0000-0000-000000000000" # "eeda5230-55b2-4b74-b2f0-d4fd37711bba"

api.get 'sql-data-dumps' bind @ .then run