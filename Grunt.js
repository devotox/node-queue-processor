'use strict';

module.exports = function(grunt) {
    grunt.Config = {
        test: {
            node: {
                src: ['build/test/node.js']                
            }
        }
    };

    grunt.registerTask(
        'node',
        'Perform tasks necessary for building FS-Node',
        ['clean:node', 'livescript:node', 'coffee:node', 'copy:node']
    );
};
